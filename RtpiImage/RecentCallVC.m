//
//  RecentCallVC.m
//  MyApp
//
//  Created by Tanumoy on 26/07/13.
//  Copyright (c) 2013 Tanumoy. All rights reserved.
//

#import "RecentCallVC.h"
#import "CustomCellForPhoneNumberCell.h"
#import "MyAlertView.h"
#import "ContactDetailsVC.h"
#import "MBProgressHUD.h"
#import "Global.h"
#import <Parse/Parse.h>
#import "MyTestAlert.h"
#import "CustomClassForFormatString.h"
#import <CoreTelephony/CTCall.h>
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <QuartzCore/QuartzCore.h>
#import "Parser.h"
#import "callList.h"
#import "HMDiallingCode.h"
#import "Parser.h"
#import "SCAdViewController.h"
#import <Netmera/Netmera.h>
#import "ADManager.h"
#import "AppDelegate.h"
#import "SMSManager.h"
#import "UIButton+WebCache.h"
#import "EGORefreshTableHeaderView.h"
#import "DFPInterstitial.h"
#import "GADInterstitialDelegate.h"
#import "ADMobCustomClass.h"
#import "SuprcallNetworkCheck.h"
#import "SendCallDetailsPost.h"
#import "MyAddressbook.h"

#define kAlertText 26
#define kBackgroundQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

@interface RecentCallVC ()<callListDelegate,UIAlertViewDelegate,smsDelegate,GADInterstitialDelegate,EGORefreshTableHeaderDelegate>
{
    NSArray *arrayOutgoingCalls,*arrayIncomingCalls;
    NSString *phnNumToCall;
    UITapGestureRecognizer* singleTap;
    NSURL *phoneURL;
    BOOL allowToCallUrl,_reloading;
    EGORefreshTableHeaderView *_refreshHeaderView;
    GADInterstitial *interstitialGAD;
    DFPInterstitial *interstitialDFP;
    BOOL isAlertShown;
}
@end

@implementation RecentCallVC;
@synthesize dataSource;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    allowToCallUrl = YES;
    
    SuprcallNetworkCheck *netWorkCheck = [[SuprcallNetworkCheck alloc]init];
    
    if(![netWorkCheck isActiveNetworkAvailable])
    {
        NSString *errorMessage;
        ([globalLanguage isEqualToString:@"tr"])?(errorMessage = @"Lütfen internet bağlantınızı kontrol edin."):(errorMessage = @"Please check your internet connectivity.");
        UIAlertView *alertNet = [[UIAlertView alloc]initWithTitle:@"Error" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertNet show];
        return;
        
    }
    // [self performSelector:@selector(loadAdIfNeeded) withObject:nil afterDelay:6];
    //    if(reloadingNeeded)
    //    {
    //        [self getCallList];
    //    }
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(didReceiveRemoteNotification:)
     name:@"ReceivedPush"
     object:nil];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //     selector:@selector(appHasGoneInBackgroundFromContactsScreen)
    //         name:UIApplicationDidEnterBackgroundNotification
    //       object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appHasComeinForegroundFromPhoneScreen)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(didCallStatusChangeStarted:)
     name:@"CTCallStateDidChangeStarted"
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(didCallStatusChangeEnded:)
     name:@"CTCallStateDidChangeEnded"
     object:nil];
    
    [[[[[self tabBarController] tabBar] items] objectAtIndex:1] setBadgeValue:0];
}

-(void)didCallStatusChangeStarted:(NSDictionary *)userInfo
{
    if ([strCallState isEqualToString:@"CTCallStateDialing"])
    {
        //NSLog(@"CallStarted");
        callingStateRCVC=@"CallStarted";
        NSDateFormatter *df = [[NSDateFormatter alloc] init] ;
        [df setDateFormat:@"HH:mm:ss"];
        NSDate *now = [NSDate date];
        backGroundDate= now;
        backGroundDate=[self correctedFormat:backGroundDate];
        
        NSDateFormatter *dfNew = [[NSDateFormatter alloc] init];
        [dfNew setDateFormat:@"dd MMM, YYYY"];
        @try {
            strDateFinal=[dfNew stringFromDate:now];
        }
        @catch (NSException *exception) {
        }
    }
}

-(void)didCallStatusChangeEnded:(NSDictionary *)userInfo
{
    reloadingNeeded = YES;
    
    MyAddressbook *address = [[MyAddressbook alloc]init];
    [address deleteContactWithOutNames];
    [self getCallList];
    
    /*
     if (!allowToCallUrl) { // for a non suprcall user if sms is not sent, then no need to call the url;
     [HUD hide:YES];
     allowToCallUrl = YES;
     return;
     }
     */
    
    if ([strStatusForCall isEqualToString:@"Started"] && [callingStateRCVC isEqualToString:@"CallStarted"])
    {
        callingStateRCVC=@"callend";
        strStatusForCall=@"";
        NSDateFormatter *df = [[NSDateFormatter alloc] init] ;
        [df setDateFormat:@"HH:mm:ss"];
        NSDate *now = [NSDate date];
        
        ForegroundDate= now;
        ForegroundDate=[self correctedFormat:ForegroundDate];
        
        HUD = [[MBProgressHUD alloc]initWithView:self.view];
        if ([globalLanguage isEqualToString:@"tr"]) {
            HUD.labelText = @"Lütfen bekleyin...";
        }
        else
            HUD.labelText = @"Please wait...";
        HUD.mode = MBProgressHUDModeIndeterminate;
        [HUD show:YES];
        [self.view addSubview:HUD];
        
        // NSLog(@"DIAL:%@",strSelectedNumber);
        // NSLog(@"FDATE==%@ ANAD BDATE==%@",ForegroundDate,backGroundDate);
        
        NSTimeInterval interval = [ForegroundDate timeIntervalSinceDate:backGroundDate];
        NSInteger ti = (NSInteger)interval;
        
        int myInt = ti;
        //        if (myInt<7.0) {
        //            ti=(int)myInt;
        //        }
        //        else if(myInt==7.0)
        //        {
        //            ti=0;
        //        }
        //        else
        //        {
        //            myInt=myInt-7.0;
        //            ti=(int)myInt;
        //        }
        if(myInt < 12.0)
        {
            ti=0;
        }
        else
        {
            myInt=myInt-12.0;
            ti=(int)myInt;
        }
        
        NSInteger seconds = ti % 60;
        if(ti < 20)
        {
            ti = 0;
            seconds = 0;
        }
        NSInteger minutes = (ti / 60) % 60;
        NSInteger hours = (ti / 3600);
        NSString *strHour=[NSString stringWithFormat:@"%02i",hours];
        NSString *strMinutes=[NSString stringWithFormat:@"%02i",minutes];
        NSString *strSecs=[NSString stringWithFormat:@"%02i",seconds];
        
        NSString *strResult;
        
        if ([strHour intValue]==0 && [strMinutes intValue]==0) {
            strResult=[NSString stringWithFormat:@"%@s",strSecs];
        }
        else if ([strHour intValue]==0 && [strMinutes intValue]!=0)
        {
            strResult=[NSString stringWithFormat:@"%@m %@s",strMinutes,strSecs];
        }
        else
            strResult=[NSString stringWithFormat:@"%@h %@m %@s",strHour,strMinutes,strSecs];
        
        strDuration=strResult;
        
        //NSLog(@"interval==%@",strResult);
        
        
        defaultRegisteredNum = [NSUserDefaults standardUserDefaults];
        
        NSString *startdate=[NSString stringWithFormat:@"%@",backGroundDate];
        NSString *enddate=[NSString stringWithFormat:@"%@",ForegroundDate];
        
        
        if(phnNumToCall.length < 6)
        {
            return;
        }
        
        CTTelephonyNetworkInfo *telephonyInfo = [CTTelephonyNetworkInfo new];
        NSLog(@"Current Radio Access Technology: %@", telephonyInfo.subscriberCellularProvider.carrierName);
        
        //  NSString *localURL=[[NSString alloc] initWithFormat:@"data=calldetailscall&userNum=%@&userCountryCode=%@&dialledNum=%@&receiverCountryCode=%@&Lat=%@&Long=%@&Duration=%@&startTime=%@&endTime=%@&callStatus=%@&gsmOperator=%@",[defaultRegisteredNum objectForKey:@"Number"],[defaultsCountryCode objectForKey:@"countryCode"],phnNumToCall,phoneCode,[NSString stringWithFormat:@"%f",value4Lat],[NSString stringWithFormat:@"%f",value4Long],strDuration,startdate,enddate,@"push+startCall",telephonyInfo.subscriberCellularProvider.carrierName];
        
        if ([phnNumToCall hasPrefix:@"+"]) {
            NSLog(@"Entered formatedPhoneNumber");
            NSString *strPhnWithoutPlus = [phnNumToCall substringFromIndex:1];
            phnNumToCall = [NSString stringWithFormat:@"~%@",strPhnWithoutPlus];
        }
        
        NSDictionary *dictAfterCall = [NSDictionary dictionaryWithObjectsAndKeys:phnNumToCall,@"dialledNum",phoneCode,@"receiverCountryCode",strDuration,@"Duration",startdate,@"startTime",enddate,@"endTime",telephonyInfo.subscriberCellularProvider.carrierName,@"gsmOperator", nil];
        
        [SendCallDetailsPost sendCallDetailsAfterCall:dictAfterCall];
        
        //  localURL=[localURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"url = %@",localURL);
        
        //[self getUrlAndParseForCallDetailsAfterCall:baseUrl :localURL];
        //        if(switchIsOn)
        //        {
        //            isShowAd = YES;
        //           // self.view.userInteractionEnabled = NO;
        //            [self performSelector:@selector(showAdAccordingly) withObject:nil afterDelay:3];
        //        }
        
        
    }
}

-(void)getUrlAndParseForEdit:(NSString *)strGlobalUrl :(NSString *)urlEndPart
{
    
    NSString *completeUrlString=[NSString stringWithFormat:@"%@%@",strGlobalUrl,urlEndPart];
    NSLog(@"complete==%@",completeUrlString);
    NSURL *completeUrl=[NSURL URLWithString:completeUrlString];
    
    dispatch_async(kBackgroundQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL:completeUrl];
        [self performSelectorOnMainThread:@selector(fetchedDataForProfile:) withObject:data waitUntilDone:YES];
    });
    
}

- (void)fetchedDataForProfile:(NSData *)responseDataTest
{
    //parse out the json data
    
    if(responseDataTest)
    {
        NSError* error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseDataTest //1
                                                             options:kNilOptions
                                                               error:&error];
        if(error)
        {
            
            [HUD hide:YES];
            if ([globalLanguage isEqualToString:@"tr"])
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"hata" message:@"SuprCall beklenmedik bir hata ile karşılaştı, lütfen internet bağlantınızı kontrol ediniz.." delegate:self cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            else
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"SuprCall encountered an unexpected error, please verify your wifi or internet connection is working properly." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            
            
        }
        else
        {
            
            //NSLog(@"JSON==%@",json);
            dictionaryUserDetails=[json objectForKey:@"Profile Details"];
            strGlobalUserName=[NSString stringWithFormat:@"%@ %@",[dictionaryUserDetails objectForKey:@"First Name"],[dictionaryUserDetails objectForKey:@"Last Name"]];
            
            //NSLog(@"My Prof: %@",dictionaryUserDetails);
            
        }
    }
    else
    {
        if ([globalLanguage isEqualToString:@"tr"])
        {
            UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"hata" message:@"SuprCall beklenmedik bir hata ile karşılaştı, lütfen internet bağlantınızı kontrol ediniz.." delegate:self cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
            [noConnectionAlert show];
        }
        else
        {
            UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"SuprCall encountered an unexpected error, please verify your wifi or internet connection is working properly." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [noConnectionAlert show];
        }
    }
    
    [HUD hide:YES];
}

-(void)didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // see http://stackoverflow.com/a/2777460/305149
    
    //    if (strCallingNumber!=nil)
    //    {
    NSLog(@"strCallingNumber inside %@",strCallingNumber);
    NSString *receiverNumber = strCallingNumber ? :[[NSUserDefaults standardUserDefaults]objectForKey:@"pushNumber"];
    defaultRegisteredNum = [NSUserDefaults standardUserDefaults];
    //    if ([strCallingNumber isEqualToString:[defaultRegisteredNum objectForKey:@"Number"]]) {
    //        return;
    //    }
    [arrOfIncomingProfile addObject:receiverNumber];
    NSLog(@"strCallingNumber inside %@",receiverNumber);

    [self getUrlAndParseForTestData:[NSString stringWithFormat:@"%@point.php?data=viewerdetails&phnNum=%@&toPhnNum=%@",kBaseUrl,receiverNumber,[defaultRegisteredNum objectForKey:@"Number"]]];
    //  strCallingNumber=nil;
    //    }
    //    else
    //        return;
    
}

-(void)handleSingleTap:(UITapGestureRecognizer *)gr {
    
    UIView *v = [self.view.subviews lastObject];
    [v removeFromSuperview];
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    if (currentInstallation.badge != 0) {
        currentInstallation.badge = 0;
        [currentInstallation saveEventually];
    }
}

-(void)getUrlAndParseForViewedProfile:(NSString *)url
{
    // NSURL *completeUrl=[NSURL URLWithString:url];
    // NSData* data = [NSData dataWithContentsOfURL:completeUrl];
    [arrOfIncomingProfile removeLastObject];
    //NSLog(@"data=%@",data);
    
}

-(void)appHasComeinForegroundFromPhoneScreen
{
    //[self viewWillAppear:YES];
    //NSLog(@"III==%@",arrOfIncomingProfile);
    
    if ([arrOfMissedProfileNumber count]!=0) {
        [[[[[self tabBarController] tabBar] items] objectAtIndex:1] setBadgeValue:[NSString stringWithFormat:@"%d",[arrOfMissedProfileNumber count]]];
    }
    //[self writeToTextFile];
}

-(void)getUrlAndParseForSendSms:(NSString *)url
{
    NSURL *completeUrl=[NSURL URLWithString:url];
    NSLog(@"URL===%@",url);
    dispatch_async(kBackgroundQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL:completeUrl];
        [self performSelectorOnMainThread:@selector(fetchedDataForSendSms:) withObject:data waitUntilDone:YES];
    });
}

- (void)fetchedDataForSendSms:(NSData *)responseData
{
    if(responseData)
    {
        NSError* error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData //1
                                                             options:kNilOptions
                                                               error:&error];
        if (error) {
            
            if ([globalLanguage isEqualToString:@"tr"])
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"hata" message:@"SuprCall beklenmedik bir hata ile karşılaştı, lütfen internet bağlantınızı kontrol ediniz.." delegate:self cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            else
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"SuprCall encountered an unexpected error, please verify your wifi or internet connection is working properly." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            
            [HUD hide:YES];
            
            return;
        }
        else
        {
            [HUD hide:YES];
            //[self performSelector:@selector(callPhone:) withObject:strSelectedNumber afterDelay:2];
            NSLog(@"XC===>%@",json);
        }
    }
}

-(void)getUrlAndParseForCallDetails:(NSString *)strGlobalUrl :(NSString *)urlEndPart
{
    NSString *completeUrlString=[NSString stringWithFormat:@"%@%@",strGlobalUrl,urlEndPart];
    NSLog(@"complete==%@",completeUrlString);
    NSURL *completeUrl=[NSURL URLWithString:completeUrlString];
    
    
    NSData* data = [NSData dataWithContentsOfURL:completeUrl];
    [self performSelectorOnMainThread:@selector(fetchedDataForCallDetails:) withObject:data waitUntilDone:YES];
}

-(void)getUrlAndParseForCallDetailsAfterCall:(NSString *)strGlobalUrl :(NSString *)urlEndPart
{
    NSString *completeUrlString=[NSString stringWithFormat:@"%@%@",strGlobalUrl,urlEndPart];
    NSLog(@"complete==%@",completeUrlString);
    NSURL *completeUrl=[NSURL URLWithString:completeUrlString];
    
    dispatch_async(kBackgroundQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL:completeUrl];
        [self performSelectorOnMainThread:@selector(fetchedDataForCallDetailsAfterCall:) withObject:data waitUntilDone:YES];
    });
}

- (void)fetchedDataForCallDetails:(NSData *)responseData
{
    if(responseData)
    {
        [self getCallList];
        NSError* error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData //1
                                                             options:kNilOptions
                                                               error:&error];
        if (error) {
            
            if ([globalLanguage isEqualToString:@"tr"])
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"hata" message:@"SuprCall beklenmedik bir hata ile karşılaştı, lütfen internet bağlantınızı kontrol ediniz.." delegate:self cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            else
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"SuprCall encountered an unexpected error, please verify your wifi or internet connection is working properly." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            
            [HUD hide:YES];
            
            return;
        }
        else
        {
            [HUD hide:YES];
            // [self performSelector:@selector(callPhone:) withObject:strSelectedNumber afterDelay:2];
            NSLog(@"XC===>%@",json);
        }
    }
}

- (void)fetchedDataForCallDetailsAfterCall:(NSData *)responseData
{
    if(responseData)
    {
        [self getCallList];
        NSError* error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData //1
                                                             options:kNilOptions
                                                               error:&error];
        if (error) {
            
            if ([globalLanguage isEqualToString:@"tr"])
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"hata" message:@"SuprCall beklenmedik bir hata ile karşılaştı, lütfen internet bağlantınızı kontrol ediniz.." delegate:self cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            else
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"SuprCall encountered an unexpected error, please verify your wifi or internet connection is working properly." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            
            [HUD hide:YES];
            
            return;
        }
        else
        {
            [HUD hide:YES];
            //[self performSelector:@selector(callPhone:) withObject:strSelectedNumber afterDelay:2];
            NSLog(@"XC===>%@",json);
        }
    }
}

-(void)getUrlAndParseForTestData:(NSString *)url
{
    NSURL *completeUrl=[NSURL URLWithString:url];
    NSLog(@"getUrlAndParseForTestData Cmpleteurl =%@",url);
    NSData* data = [NSData dataWithContentsOfURL:completeUrl];
    [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
    
}

- (void)fetchedData:(NSData *)responseData
{
    if(responseData)
    {
        NSError* error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData //1
                                                             options:kNilOptions
                                                               error:&error];
        if (error) {
            
            if ([globalLanguage isEqualToString:@"tr"])
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"hata" message:@"SuprCall beklenmedik bir hata ile karşılaştı, lütfen internet bağlantınızı kontrol ediniz.." delegate:self cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            else
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"SuprCall encountered an unexpected error, please verify your wifi or internet connection is working properly." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            
            //[HUD setHidden:YES];
            
            return;
        }
        else
        {
            //NSLog(@"TEST JSON===%@",json);
            
            NSDictionary *dicTemp=[json objectForKey:@"Viewer Details"];
            
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if (iOSDeviceScreenSize.height == 568)
            {
                if ([globalLanguage isEqualToString:@"tr"])
                {
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_Turk" bundle: nil];
                    MyTestAlert *controller = (MyTestAlert *)[mainStoryboard instantiateViewControllerWithIdentifier:@"MyTestAlert"];
                    controller.dicDetailsNew=dicTemp;
                    
                    [self.view addSubview:controller.view];
                    
                    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
                    
                    singleTap.numberOfTapsRequired = 1;
                    singleTap.numberOfTouchesRequired = 1;
                    [controller.view addGestureRecognizer: singleTap];
                    
                }else
                {
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
                    MyTestAlert *controller = (MyTestAlert *)[mainStoryboard instantiateViewControllerWithIdentifier:@"MyTestAlert"];
                    controller.dicDetailsNew=dicTemp;
                    
                    [self.view addSubview:controller.view];
                    
                    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
                    
                    singleTap.numberOfTapsRequired = 1;
                    singleTap.numberOfTouchesRequired = 1;
                    [controller.view addGestureRecognizer: singleTap];
                }
                
                
            }
            else
            {
                if ([globalLanguage isEqualToString:@"tr"])
                {
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone4_Turk" bundle: nil];
                    MyTestAlert *controller = (MyTestAlert *)[mainStoryboard instantiateViewControllerWithIdentifier:@"MyTestAlert"];
                    controller.dicDetailsNew=dicTemp;
                    
                    [self.view addSubview:controller.view];
                    
                    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
                    
                    singleTap.numberOfTapsRequired = 1;
                    singleTap.numberOfTouchesRequired = 1;
                    [controller.view addGestureRecognizer: singleTap];
                }
                else
                {
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone4" bundle: nil];
                    MyTestAlert *controller = (MyTestAlert *)[mainStoryboard instantiateViewControllerWithIdentifier:@"MyTestAlert"];
                    controller.dicDetailsNew=dicTemp;
                    
                    [self.view addSubview:controller.view];
                    
                    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
                    
                    singleTap.numberOfTapsRequired = 1;
                    singleTap.numberOfTouchesRequired = 1;
                    [controller.view addGestureRecognizer: singleTap];
                }
                
            }
            
            defaultsCountryCode = [NSUserDefaults standardUserDefaults];
            defaultRegisteredNum = [NSUserDefaults standardUserDefaults];
            [self getCallList];
            
            NSString *strNum = strCallingNumber;
            NSLog(@"strNum recent %@",strCallingNumber);
            
            if (strNum.length>10) {
                if ([strNum hasPrefix:@"+"]) {
                    NSString *strPhnWithoutPlus = [strNum substringFromIndex:1];
                    strNum = [NSString stringWithFormat:@"~%@",strPhnWithoutPlus];

                }else{
                    strNum = [NSString stringWithFormat:@"~%@",strNum];
                    NSLog(@"Entered formatedPhoneNumber %@",strNum);
                }
            }
            
            dispatch_async(kBackgroundQueue, ^{
                [self getUrlAndParseForViewedProfile:[NSString stringWithFormat:@"%@point.php?data=viewedProfile&number=%@&pushStatus=1&userNumber=%@&userCountryCode=%@",kBaseUrl,strNum,[defaultRegisteredNum objectForKey:@"Number"],[defaultsCountryCode objectForKey:@"countryCode"]]];
            });
            
        }
    }
}

-(void)getUrlAndParseForRecentCallList:(NSString *)strGlobalUrl :(NSString *)urlEndPart
{
    NSString *completeUrlString=[NSString stringWithFormat:@"%@%@",strGlobalUrl,urlEndPart];
    NSLog(@"complete==%@",completeUrlString);
    NSURL *completeUrl=[NSURL URLWithString:completeUrlString];
    
    dispatch_async(kBackgroundQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL:completeUrl];
        [self performSelectorOnMainThread:@selector(fetchedDataForRecentCall:) withObject:data waitUntilDone:YES];
    });
    
}

- (void)fetchedDataForRecentCall:(NSData *)responseData
{
    if(responseData)
    {
        NSError* error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData //1
                                                             options:kNilOptions
                                                               error:&error];
        if (error) {
            
            if ([globalLanguage isEqualToString:@"tr"])
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"hata" message:@"SuprCall beklenmedik bir hata ile karşılaştı, lütfen internet bağlantınızı kontrol ediniz.." delegate:self cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            else
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"SuprCall encountered an unexpected error, please verify your wifi or internet connection is working properly." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            
            //[HUD setHidden:YES];
            
            return;
        }
        else
        {
            //NSLog(@"TEST JSON===%@",json);
            dicOutGoing=[json objectForKey:@"Outgoingcalls"];
            dicIncoming=[json objectForKey:@"Incomingcalls"];
            arrOfOutgoingCall=[dicOutGoing objectForKey:@"Receiver Number"];
            arrOfIncomingCall=[dicIncoming objectForKey:@"Caller Number"];
            
            arrOfInDuration=[dicIncoming objectForKey:@"Call Status"];
            arrOfOutDuration=[dicOutGoing objectForKey:@"Call Status"];
            
            arrOfStartOutTime=[dicOutGoing objectForKey:@"Call Start Time"];
            arrOfStartInTime =[dicIncoming objectForKey:@"Call Start Time"];
            
            NSLog(@"TEST dicOutGoing===%@",arrOfOutDuration);
            
            /* NSLog(@"TEST dicOutGoing===%@",dicOutGoing);
             NSLog(@"TEST dicIncoming===%@",dicIncoming);
             NSLog(@"TEST dicOutGoing===%@",arrOfStartOutTime);*/
            
            @try {
                if ([dicOutGoing count]!=0)
                {
                    NSMutableArray *arrTemp=[[NSMutableArray alloc]initWithArray:arrOfOutgoingCall];
                    NSMutableArray *arrTemp1=[[NSMutableArray alloc]initWithArray:arrOfOutDuration];
                    NSMutableArray *arrTemp2=[[NSMutableArray alloc]initWithArray:arrOfStartOutTime];
                    
                    int n=[arrTemp count];
                    for (int m=0; m < n; m++) {
                        //NSLog(@"[arrTemp objectAtIndex:m]==%@",[arrTemp objectAtIndex:m]);
                        if ([[arrTemp objectAtIndex:m] isEqualToString:@"(null)"]) {
                            NSLog(@"enter");
                            [arrTemp removeObjectAtIndex:m];
                            [arrTemp insertObject:@"" atIndex:m];
                            
                            [arrTemp1 removeObjectAtIndex:m];
                            [arrTemp1 insertObject:@"" atIndex:m];
                            
                            [arrTemp2 removeObjectAtIndex:m];
                            [arrTemp2 insertObject:@"" atIndex:m];
                            
                            //[arrOfOutDuration removeObjectAtIndex:m];
                            //[arrOfStartOutTime removeObjectAtIndex:m];
                        }
                    }
                    [arrTemp removeObject:@""];[arrTemp1 removeObject:@""];[arrTemp2 removeObject:@""];
                    arrOfOutgoingCall=[[NSMutableArray alloc]initWithArray:arrTemp ];
                    arrOfOutDuration=[[NSMutableArray alloc] initWithArray:arrTemp1];
                    arrOfStartOutTime=[[NSMutableArray alloc] initWithArray:arrTemp2];
                    
                    //NSLog(@"TEST dicOutGoing===%@",arrOfStartOutTime);
                }
                if ([dicIncoming count]!=0)
                {
                    NSMutableArray *arrITemp=[[NSMutableArray alloc]initWithArray:arrOfIncomingCall];
                    NSMutableArray *arrITemp1=[[NSMutableArray alloc]initWithArray:arrOfInDuration];
                    NSMutableArray *arrITemp2=[[NSMutableArray alloc]initWithArray:arrOfStartInTime];
                    
                    int p=[arrITemp count];
                    for (int m=0; m < p; m++) {
                        if ([[arrITemp objectAtIndex:m] isEqualToString:@"(null)"]) {
                            NSLog(@"enter");
                            [arrITemp removeObjectAtIndex:m];
                            [arrITemp insertObject:@"" atIndex:m];
                            
                            [arrITemp1 removeObjectAtIndex:m];
                            [arrITemp1 insertObject:@"" atIndex:m];
                            
                            [arrITemp2 removeObjectAtIndex:m];
                            [arrITemp2 insertObject:@"" atIndex:m];
                            
                        }
                    }
                    [arrITemp removeObject:@""];
                    [arrITemp1 removeObject:@""];
                    [arrITemp2 removeObject:@""];
                    arrOfIncomingCall=[[NSMutableArray alloc]initWithArray:arrITemp];
                    arrOfInDuration=[[NSMutableArray alloc] initWithArray:arrITemp1];
                    arrOfStartInTime=[[NSMutableArray alloc] initWithArray:arrITemp2];
                    
                }
                
                [HUD hide:YES];
                
                NSString *trimmedString;
                if ([arrOfOutgoingCall count]!=0)
                {
                    for (int j=0; j<[arrOfOutgoingCall count]; j++)
                    {
                        isFound=NO;
                        if ([[arrOfOutgoingCall objectAtIndex:j] length]>7)
                        {
                            trimmedString=[[arrOfOutgoingCall objectAtIndex:j] substringFromIndex:[[arrOfOutgoingCall objectAtIndex:j] length]-6];
                        }
                        else
                        {
                            trimmedString=[[arrOfOutgoingCall objectAtIndex:j] substringFromIndex:[[arrOfOutgoingCall objectAtIndex:j] length]-[[arrOfOutgoingCall objectAtIndex:j] length]];
                        }
                        
                        for (int i=0; i<[arrOfFormatedMobNum count]; i++)
                        {
                            if ([[arrOfFormatedMobNum objectAtIndex:i]length]>7)
                            {
                                if ([[[arrOfFormatedMobNum objectAtIndex:i] substringFromIndex:[[arrOfFormatedMobNum objectAtIndex:i]length]-6] isEqualToString:trimmedString])
                                {
                                    [arrFinal addObject:[arrOfName objectAtIndex:i]];
                                    isFound=YES;
                                    break;
                                }
                            }
                            else
                            {
                                if ([[[arrOfFormatedMobNum objectAtIndex:i] substringFromIndex:[[arrOfFormatedMobNum objectAtIndex:i]length]-[[arrOfFormatedMobNum objectAtIndex:i]length]] isEqualToString:trimmedString])
                                {
                                    [arrFinal addObject:[arrOfName objectAtIndex:i]];
                                    isFound=YES;
                                    break;
                                }
                            }
                            
                        }
                        if (!isFound) {
                            
                            if ([globalLanguage isEqualToString:@"tr"]) {
                                
                                [arrFinal addObject:@"İsimsiz"];
                            }
                            else
                                [arrFinal addObject:@"No name"];
                        }
                        
                    }
                }
                if ([arrOfIncomingCall count]!=0)
                {
                    for (int j=0; j<[arrOfIncomingCall count]; j++)
                    {
                        isFoundIn=NO;
                        NSString *trimmedString;
                        if ([[arrOfIncomingCall objectAtIndex:j] length]>7)
                        {
                            trimmedString=[[arrOfIncomingCall objectAtIndex:j] substringFromIndex:[[arrOfIncomingCall objectAtIndex:j] length]-6];
                        }
                        else
                        {
                            trimmedString=[[arrOfIncomingCall objectAtIndex:j] substringFromIndex:[[arrOfIncomingCall objectAtIndex:j] length]-[[arrOfIncomingCall objectAtIndex:j] length]];
                        }
                        //NSLog(@"arrOfFormatedMobNum==%@",arrOfFormatedMobNum);
                        for (int i=0; i<[arrOfFormatedMobNum count]; i++)
                        {
                            if ([[arrOfFormatedMobNum objectAtIndex:i]length]>7)
                            {
                                if ([[[arrOfFormatedMobNum objectAtIndex:i] substringFromIndex:[[arrOfFormatedMobNum objectAtIndex:i]length]-6] isEqualToString:trimmedString])
                                {
                                    [arrFinalIncoming addObject:[arrOfName objectAtIndex:i]];
                                    [arrOfIncomingCall removeObjectAtIndex:j];
                                    [arrOfIncomingCall insertObject:[arrOfFormatedMobNum objectAtIndex:i] atIndex:j];
                                    isFoundIn=YES;
                                    
                                    //NSLog(@"IN==%@",arrOfIncomingCall);
                                    break;
                                }
                            }
                            else
                            {
                                if ([[[arrOfFormatedMobNum objectAtIndex:i] substringFromIndex:[[arrOfFormatedMobNum objectAtIndex:i]length]-[[arrOfFormatedMobNum objectAtIndex:i]length]] isEqualToString:trimmedString])
                                {
                                    [arrFinalIncoming addObject:[arrOfName objectAtIndex:i]];
                                    
                                    [arrOfIncomingCall removeObjectAtIndex:j];
                                    [arrOfIncomingCall insertObject:[arrOfFormatedMobNum objectAtIndex:i] atIndex:j];
                                    
                                    isFoundIn=YES;
                                    break;
                                }
                            }
                            
                        }
                        if (!isFoundIn) {
                            if ([globalLanguage isEqualToString:@"tr"]) {
                                
                                [arrFinalIncoming addObject:@"İsimsiz"];
                            }
                            else
                                [arrFinalIncoming addObject:@"No name"];
                        }
                        
                    }
                }
                
            }
            @catch (NSException *exception)
            {
                /*UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"SuprCall encountered an unexpected error,%@",exception] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 [noConnectionAlert show];*/
                NSLog(@"%@",exception);
                //NSLog(@"%@",exception);
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"No Data to display"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [noConnectionAlert show];
                [HUD hide:YES];
            }
            @finally {
            }
            [self.tblRecentCall reloadData];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showAdAccordingly];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationItem setTitle:@"SuprCall"];
    if(floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:210.0/255.0 green:20.0/255.0 blue:0.0/255.0 alpha:1.0]];
    }
    else
    {
        [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:210.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0]];
        [self.navigationController.navigationBar setTranslucent:NO];
        
    }
    
    strStatus=@"out";
    HUD = [[MBProgressHUD alloc]initWithView:self.view];
    ([globalLanguage isEqualToString:@"tr"])?(HUD.labelText = @"Yükleniyor"):(HUD.labelText = @"Loading");
    [self.view addSubview:HUD];
    [HUD show:YES];
    [self getCallList];
    
    UIBarButtonItem *leftbutton = [[UIBarButtonItem alloc]
                                   initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                   target:self
                                   action:@selector(callListFunction)];
    self.navigationItem.leftBarButtonItem = leftbutton;
    
    //    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getCallList) name:@"getCallList" object:nil];
    
    if (_refreshHeaderView == nil) {
        
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tblRecentCall.bounds.size.height, self.view.frame.size.width, _tblRecentCall.bounds.size.height)];
        [view setBackgroundColor:[UIColor clearColor]];
        view.delegate = self;
        [_tblRecentCall addSubview:view];
        _refreshHeaderView = view;
    }
    
    // Do any additional setup after loading the view.
}

- (void)callListFunction
{
    ([globalLanguage isEqualToString:@"tr"])?(HUD.labelText = @"Yükleniyor"):(HUD.labelText = @"Loading");
    [HUD show:YES];
    [self getCallList];
}

-(void)getCallList
{
    // [HUD show:YES];
    callList *list = [[callList alloc]init];
    [list setDelegate: self];
    [list getRecentCallsForNumber:[defaultRegisteredNum objectForKey:@"Number"]WithCountryCode:[defaultRegisteredNum objectForKey:@"countryCode"]];
    //http://www.suprcall.com/suprcall/point.php?data=recentCalls&phnNum=5326476494&countryCode=90
    //[list getRecentCallsForNumber:@"5326476494" WithCountryCode:@"90"];
    // [list getRecentCallsForNumber:@"5066603239" WithCountryCode:@"90"];
}

#pragma mark SMS Delegates

-(void)didSentSmsToNumber:(NSString *)phoneNumber
{
    allowToCallUrl = YES;
    phnNumToCall = phoneNumber;
    [self callAction:phoneNumber];
}

-(void)didCancelledSmsToNumber:(NSString *)phoneNumber
{
    allowToCallUrl = NO;
    phnNumToCall = phoneNumber;
    [self callAction:phoneNumber];
}

#pragma mark - CallList Delegates

-(void)myIncomingCalls:(NSArray *)_array
{
    reloadingNeeded = NO;
    [HUD hide:YES];
    arrayIncomingCalls = [[NSMutableArray alloc]initWithArray:_array];
    //    [arrayIncomingCalls enumerateObjectsUsingBlock:^(callList *obj, NSUInteger idx, BOOL *stop) {
    //        NSLog(@"arrayIncomingCalls ======= %@",obj.name);
    //    }];
    if(arrayIncomingCalls.count >0 && ![strStatus isEqualToString:@"out"])
    {
        NSLog(@"Entered arrayIncomingCalls reloadData");
        [self.tblRecentCall reloadData];
    }
    [self doneLoadingTableViewData];
}

-(void)myOutgoingCalls:(NSArray *)_array
{
    reloadingNeeded = NO;
    [HUD hide:YES];
    arrayOutgoingCalls = [[NSMutableArray alloc]initWithArray:_array];
    //    [arrayOutgoingCalls enumerateObjectsUsingBlock:^(callList *obj, NSUInteger idx, BOOL *stop) {
    //        NSLog(@"arrayOutgoingCalls ======= %@",obj.number);
    //    }];
    if(arrayOutgoingCalls.count >0 && [strStatus isEqualToString:@"out"])
    {
        NSLog(@"Entered arrayOutgoingCalls reloadData");
        
        [self.tblRecentCall reloadData];
    }
    [self doneLoadingTableViewData];
}
-(void)failedtoLoad:(NSString *)failureMessage
{
    [HUD hide:YES];
    [self doneLoadingTableViewData];
    
}
#pragma mark - SBTableAlertDataSource

- (NSInteger)tableAlert:(SBTableAlert *)tableAlert numberOfRowsInSection:(NSInteger)section {
    
    return [array count];
    
    
}

- (NSInteger)numberOfSectionsInTableAlert:(SBTableAlert *)tableAlert {
    
    return 1;
}
- (CGFloat)tableAlert:(SBTableAlert *)tableAlert heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 52.0;
}
- (NSString *)tableAlert:(SBTableAlert *)tableAlert titleForHeaderInSection:(NSInteger)section {
    if (tableAlert.view.tag == 3)
        return [NSString stringWithFormat:@"Section Header %d", section];
    else
        return nil;
}

- (UITableViewCell *)tableAlert:(SBTableAlert *)tableAlert cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell;
    
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] ;
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(48,0, 210, 50)];
    NSString *strTemp=[CustomClassForFormatString formatString:[arrOfMissedProfileName objectAtIndex:indexPath.row]];
    NSString *strTempLoc=[CustomClassForFormatString formatString:[arrOfMissedLocationName objectAtIndex:indexPath.row]];
    
    if ([globalLanguage isEqualToString:@"tr"])
    {
        if ([strTempLoc isEqualToString:@""])
        {
            lbl.text=[NSString stringWithFormat:@"%@, size profilini yolladı.",strTemp];
        }
        else
            lbl.text=[NSString stringWithFormat:@"%@,%@, size profilini yolladı.",strTemp,strTempLoc];
    }else
    {
        if ([strTempLoc isEqualToString:@""])
        {
            lbl.text=[NSString stringWithFormat:@"%@, sent you profile.",strTemp];
        }
        else
        {
            lbl.text=[NSString stringWithFormat:@"%@,%@, sent you profile.",strTemp,strTempLoc];
        }
    }
    
    
    
    lbl.textColor=[UIColor darkGrayColor];
    lbl.numberOfLines=3;
    lbl.lineBreakMode=NSLineBreakByCharWrapping;
    
    lbl.adjustsFontSizeToFitWidth=YES;
    lbl.font=[UIFont fontWithName:@"Helvetica-Bold" size:14.0];
    [cell.contentView addSubview:lbl];
    
    UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(3,6, 40, 40)];
    imv.image=[UIImage imageNamed:@"suprcallUser.png"];
    
    
    [cell.contentView addSubview:imv];
    
    return cell;
}


#pragma mark - SBTableAlertDelegate

- (void)tableAlert:(SBTableAlert *)tableAlert didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableAlert.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}
- (void)tableAlert:(SBTableAlert *)tableAlert didDismissWithButtonIndex:(NSInteger)buttonIndex {
    NSLog(@"Dismissed: %i", buttonIndex);
    self.navigationController.tabBarItem.badgeValue=nil;
    
    defaultsCountryCode = [NSUserDefaults standardUserDefaults];
    defaultRegisteredNum = [NSUserDefaults standardUserDefaults];
    
    [self getUrlAndParseForAllViewedProfile:[NSString stringWithFormat:@"%@point.php?data=viewedAllProfile&userNumber=%@&userCountryCode=%@",kBaseUrl,[defaultRegisteredNum objectForKey:@"Number"],[defaultsCountryCode objectForKey:@"countryCode"]]];
    
}
-(void)getUrlAndParseForAllViewedProfile:(NSString *)url
{
    
    // NSURL *completeUrl=[NSURL URLWithString:url];
    // NSData* data = [NSData dataWithContentsOfURL:completeUrl];
    
    arrOfMissedProfileNumber=nil;
    arrOfMissedProfileName=nil;
    arrOfMissedProfile=nil;
    
    //NSLog(@"data=%@",data);
    
}
#pragma mark ============= TableView Delegates =====================>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if ([strStatus isEqualToString:@"out"]) {
        return [arrayOutgoingCalls count];
    }
    else
    {
        return [arrayIncomingCalls count];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIImage *myImage = [UIImage imageNamed:@"alphabeat.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:myImage];
    imageView.frame = CGRectMake(0,0,330,30);
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(9.0, -1.0, 300.0, 30)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.opaque = NO;
    
    NSString *key;
    
    if ([strStatus isEqualToString:@"out"]) {
        if ([globalLanguage isEqualToString:@"tr"]) {
            key=@"Yaptığınız Aramalar";
        }else
            key=@"Outgoing Calls";
    }
    else
    {
        if ([globalLanguage isEqualToString:@"tr"]) {
            key=@"Gelen Aramalar";
        }else
            key=@"Incoming Calls";
    }
    //NSLog(@"KEY===%@",key);
    headerLabel.text = key;
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.highlightedTextColor = [UIColor blackColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:11];
    headerLabel.shadowColor = [UIColor clearColor];
    headerLabel.shadowOffset = CGSizeMake(0.0, 1.0);
    headerLabel.numberOfLines = 0;
    headerLabel.textAlignment = NSTextAlignmentLeft;
    [imageView addSubview: headerLabel];
    
    
    return imageView;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    defaultsCountryCode = [NSUserDefaults standardUserDefaults];
    static NSString *CellIdentifier = @"Cell";
    
    CustomCellForPhoneNumberCell *cell=nil;
    if (cell==nil) {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    callList *list;
    if([strStatus isEqualToString:@"out"])
    {
        if(arrayOutgoingCalls.count >= indexPath.row)
        {
            @try {
                list = arrayOutgoingCalls[indexPath.row];
                cell.phnNum.text = list.number;
            }
            @catch (NSException *exception) {
                
            }
        }
    }
    else
    {
        if(arrayIncomingCalls.count >= indexPath.row)
        {
            @try {
                list = arrayIncomingCalls[indexPath.row];
                cell.phnNum.text = [NSString stringWithFormat:@"+%@%@",list.countryCode,list.number];
            }
            @catch (NSException *exception) {
                
            }
            
        }
        
    }
    cell.lblUserName.text = list.name;
    cell.lblDuration.text = list.date_time;
    
    if(list.imageUrl)
    {
        [cell.btCall setImageWithURL:[NSURL URLWithString:list.imageUrl] placeholderImage:[UIImage imageNamed:@"suprcallUser.png"]];
        [cell.btCall setImage:nil forState:UIControlStateHighlighted];
    }
    else
    {
        ([list.status isEqualToString:@"push"]) ? ([cell.btCall setImage:[UIImage imageNamed:@"suprcallUser.png"] forState:UIControlStateNormal]):([cell.btCall setImage:[UIImage imageNamed:@"pushCall.png"] forState:UIControlStateNormal]);
    }
    
    //    if (isShowAd) {
    //        cell.btCall.userInteractionEnabled = NO;
    //    }else{
    //        cell.btCall.userInteractionEnabled = YES;
    //    }
    
    [cell.btCall addTarget:self action:@selector(callButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.btCall.tag = indexPath.row;
    return cell;
    
}



#pragma mark - Table View Delegate Methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    callList *list;
    
    @try {
        ([strStatus isEqualToString:@"out"])?(list = arrayOutgoingCalls[indexPath.row]):(list = arrayIncomingCalls[indexPath.row]);
    }
    @catch (NSException *exception) {
        return;
    }
    
    if([list.suprcallUser isEqualToString:@"Yes"])
    {
        
        if([list.suprcallUser isEqualToString:@"Yes"])
        {
            if([[UIScreen mainScreen]bounds].size.height == 568)
            {
                if ([globalLanguage isEqualToString:@"tr"])
                {
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_Turk" bundle: nil];
                    ContactDetailsVC *controller = (ContactDetailsVC *)[mainStoryboard instantiateViewControllerWithIdentifier:@"ContactDetailsVC"];
                    
                    if([strStatus isEqualToString:@"out"])
                    {
                        controller.detailsPhn = list.number;
                    }
                    else
                    {
                        controller.detailsPhn = list.number;
                        controller.detailsCode = list.countryCode;
                    }
                    
                    [self.navigationController pushViewController:controller animated:YES];
                }
                else
                {
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
                    ContactDetailsVC *controller = (ContactDetailsVC *)[mainStoryboard instantiateViewControllerWithIdentifier:@"ContactDetailsVC"];
                    
                    if([strStatus isEqualToString:@"out"])
                    {
                        controller.detailsPhn = list.number;
                    }
                    else
                    {
                        controller.detailsPhn = list.number;
                        controller.detailsCode = list.countryCode;
                    }
                    [self.navigationController pushViewController:controller animated:YES];
                }
                
            }
            else
            {
                if ([globalLanguage isEqualToString:@"tr"])
                {
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone4_Turk" bundle: nil];
                    ContactDetailsVC *controller = (ContactDetailsVC *)[mainStoryboard instantiateViewControllerWithIdentifier:@"ContactDetailsVC"];
                    
                    if([strStatus isEqualToString:@"out"])
                    {
                        controller.detailsPhn = list.number;
                    }
                    else
                    {
                        controller.detailsPhn = list.number;
                        controller.detailsCode = list.countryCode;
                    }
                    [self.navigationController pushViewController:controller animated:YES];
                }
                else
                {
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone4" bundle: nil];
                    ContactDetailsVC *controller = (ContactDetailsVC *)[mainStoryboard instantiateViewControllerWithIdentifier:@"ContactDetailsVC"];
                    
                    if([strStatus isEqualToString:@"out"])
                    {
                        controller.detailsPhn = list.number;
                    }
                    else
                    {
                        controller.detailsPhn = list.number;
                        controller.detailsCode = list.countryCode;
                    }
                    [self.navigationController pushViewController:controller animated:YES];
                }
                
            }
            
        }
        else
        {
            if ([globalLanguage isEqualToString:@"tr"])
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"Özür Dileriz" message:[NSString stringWithFormat:@"Seçilen kişi SuprCall kullanmadığı için profil ayrıntıları görüntülenemiyor."] delegate:self cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            else{
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:[NSString stringWithFormat:@"The selected contact is not a SuprCall user. The contact’s profile details can not be displayed."] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            
        }
        
    }
    
}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    
}

#pragma mark - EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    
    [self getCallList];
    //[self startRHFetching];
    
    // [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
    
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    
    return _reloading; // should return if data source model is reloading
    
}

- (void)doneLoadingTableViewData{
    
    [_tblRecentCall setUserInteractionEnabled:YES];
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_tblRecentCall];
    
}
-(void)callButtonAction:(UIButton *)sender
{
    
    callList *list;
    //    NSIndexPath *indexPath;
    //
    //    if(floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    //    {
    //        indexPath = [self.tblRecentCall indexPathForCell:(UITableViewCell *)[[sender superview] superview]];
    //    }
    //    else
    //    {
    //        indexPath = [self.tblRecentCall indexPathForCell:(UITableViewCell *)[[[sender superview] superview] superview]];
    //    }
    if([strStatus isEqualToString:@"out"])
    {
        @try {
            list = arrayOutgoingCalls[sender.tag];
            //list = arrayOutgoingCalls[indexPath.row];
            phnNumToCall = list.number;
        }
        @catch (NSException *exception) {
            
        }
    }
    else
    {
        @try {
            list = arrayIncomingCalls[sender.tag];
            //list = arrayIncomingCalls[indexPath.row];
            phnNumToCall = [NSString stringWithFormat:@"+%@%@",list.countryCode,list.number];
        }
        @catch (NSException *exception) {
            
        }
    }
    
    
    // sms option...
    if(!list.suprcallUser)
    {
        SMSManager *manager = [SMSManager sharedManager];
        manager.delegate = self;
        manager.parentView = self;
        [manager showSMSOptionForNumber:phnNumToCall];
    }
    else
    {
        [self callAction:phnNumToCall];
    }
    
}


-(void)callAction:(id)sender
{
    callList *list;
    
    if(sender)
    {
        phnNumToCall = (NSString *)sender;
    }
    
    else
    {
        NSIndexPath *indexPath;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            indexPath = [self.tblRecentCall indexPathForCell:(UITableViewCell *)[[sender superview] superview]];
        }
        else
        {
            if(floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                indexPath = [self.tblRecentCall indexPathForCell:(UITableViewCell *)[[sender superview] superview]];
            }
            else
            {
                
                indexPath = [self.tblRecentCall indexPathForCell:(UITableViewCell *)[[[sender superview] superview] superview]];
                
            }
        }
        
        
        if([strStatus isEqualToString:@"out"])
        {
            @try {
                list = arrayOutgoingCalls[indexPath.row];
                phnNumToCall = list.number;
            }
            @catch (NSException *exception) {
            }
        }
        else
        {
            @try {
                list = arrayIncomingCalls[indexPath.row];
                phnNumToCall = [NSString stringWithFormat:@"+%@%@",list.countryCode,list.number];
                
            }
            @catch (NSException *exception) {
            }
        }
        
    }
    
    // NSString *strTemp;
    
    // Getting country code....
    NSString *interNationalCode = [phnNumToCall substringToIndex:2];
    if([interNationalCode isEqualToString:@"00"] || [phnNumToCall hasPrefix:@"+"])
    {
        HMDiallingCode *diallingCode = [[HMDiallingCode alloc]init];
        phoneCode = [diallingCode getDiallingCodeFromPhoneNumber:phnNumToCall];
    }
    else
    {
        phoneCode = [defaultRegisteredNum objectForKey:@"countryCode"];
    }
    
    NSLog(@"phoneCode = %@",phoneCode);
    
    [HUD hide:YES];
    
    NSString *cleanedString = [[phnNumToCall componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"ph number = %@",phnNumToCall);
    
    // NSString *phoneURLString = [NSString stringWithFormat:@"telprompt:%@", escapedPhoneNumber];
    strDial=escapedPhoneNumber;
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:2014];
    [comps setMonth:8];
    [comps setDay:6];
    //    NSCalendar *gregorian =
    //    [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    // NSDate *date = [gregorian dateFromComponents:comps];
    
    defaultRegisteredNum = [NSUserDefaults standardUserDefaults];
    NSString *receiver;
    NSString *formatedPhoneNumber = [[phnNumToCall componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789+"] invertedSet]] componentsJoinedByString:@""];//SERCAN
    
    [self callPhone:formatedPhoneNumber];
    
    NSString *myNumber = [defaultRegisteredNum objectForKey:@"Number"];
    NSString *firstDigit = [NSString stringWithFormat:@"%c",[phnNumToCall characterAtIndex:0]];
    
    
    if([firstDigit isEqualToString:@"0"])
    {
        NSString *cleanedString = [[[phnNumToCall substringFromIndex:1] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
        NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"escapedPhoneNumber : %@",escapedPhoneNumber);
        receiver = [NSString stringWithFormat:@"a%@%@",[defaultRegisteredNum objectForKey:@"countryCode"],escapedPhoneNumber];
    }
    
    else if([phoneCode isEqualToString:@"375"])
    {
        if([phnNumToCall hasPrefix:@"80"] && ![phnNumToCall hasPrefix:@"800"])
        {
            receiver = [NSString stringWithFormat:@"a375%@",[phnNumToCall substringFromIndex:2]];
        }
    }
    
    else if([formatedPhoneNumber hasPrefix:phoneCode])
    {
        receiver = [NSString stringWithFormat:@"a%@",formatedPhoneNumber];
    }
    else
    {
        ///////////////////////////SERCAN///////////////////////
//        if(myNumber.length != formatedPhoneNumber.length)
//        {
//            int difference = formatedPhoneNumber.length - myNumber.length;
//            if(difference >0)
//            {
//                NSString *str = [formatedPhoneNumber substringFromIndex:difference];
//                formatedPhoneNumber = str;
//            }
//        }
        ////////////////////////////////////////////////////////
        receiver = [NSString stringWithFormat:@"a%@%@",phoneCode,formatedPhoneNumber];
    }
    
    if([interNationalCode isEqualToString:@"00"])
    {
        receiver = [NSString stringWithFormat:@"a%@",[formatedPhoneNumber substringFromIndex:phoneCode.length]];
    }
    
    NSLog(@"channel receiver = %@",receiver);
    NSLog(@"channel formatedPhoneNumber = %@",formatedPhoneNumber);
    
    if(receiver.length < 7)
    {
        receiver = @"12345678";
    }
    
    PFQuery *pushQuery = [PFInstallation query];
    [pushQuery whereKey:@"channels" equalTo:receiver];
    
    defaultsCountryCode = [NSUserDefaults standardUserDefaults];
    NSLog(@"my phone number = %@",[defaultRegisteredNum objectForKey:@"Number"]);
    // NSLog(@"my name = %@",[[NSUserDefaults standardUserDefaults]stringForKey:@"fname"]);
    NSString *strPhoneNum=[defaultRegisteredNum objectForKey:@"Number"];
    
    NSString *strAlert;
    
    NSString *myName = [NSString stringWithFormat:@"%@ %@",[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"fname"]],[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"lname"]]];
    
    if ([globalLanguage isEqualToString:@"tr"])
    {
        if(myName.length < 2)
        {
            strAlert = @"Az sonra SuprCall ile aranabilirsiniz.";
        }
        else
        {
            if ([[[NSUserDefaults standardUserDefaults]stringForKey:@"location"] isEqualToString:@"(null),(null)"] || ![[NSUserDefaults standardUserDefaults]boolForKey:@"Checkbox4"])
            {
                strAlert=[NSString stringWithFormat:@"%@ %@, az sonra sizi arayabilir!",[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"fname"]],[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"lname"]]];
            }
            else
            {
                strAlert=[NSString stringWithFormat:@"%@ %@, %@, az sonra sizi arayabilir!",[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"fname"]],[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"lname"]],[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"location"]]];
            }
        }
        
    }
    else
    {
        
        if(myName.length < 2)
        {
            strAlert = @"You may recieve a SuprCall soon.";
        }
        else
        {
            if ([[[NSUserDefaults standardUserDefaults]stringForKey:@"location"] isEqualToString:@"(null),(null)"] || ![[NSUserDefaults standardUserDefaults]boolForKey:@"Checkbox4"])
            {
                strAlert=[NSString stringWithFormat:@"%@ %@, may call you soon !",[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"fname"]],[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"lname"]]];
            }
            else
            {
                strAlert=[NSString stringWithFormat:@"%@ %@, %@, may call you soon !",[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"fname"]],[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"lname"]],[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"location"]]];
            }
        }
    }
    
    strAlert = [strAlert stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    
    if([strAlert isEqualToString:@" , , may call you soon !"])
    {
        strAlert = @"You may recieve a SuprCall soon.";
    }
    else if ([strAlert isEqualToString:@" , , az sonra sizi arayabilir!"])
    {
        strAlert = @"Az sonra SuprCall ile aranabilirsiniz.";
    }
    
    
    //////////////////////////////////////////////////////////////////////////
    
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          strAlert, @"alert",
                          strPhoneNum, @"Mesage",
                          phoneCode,@"country_code",
                          @"Increment", @"badge",
                          @"cheering.caf", @"sound",
                          @"superAction",@"action",
                          nil];
    
    PFPush *push1 = [[PFPush alloc] init];
    [push1 setQuery:pushQuery]; // Set our Installation query
    //[push1 expireAtDate:date];
    [push1 expireAfterTimeInterval:60];
    [push1 setQuery:pushQuery];
    [push1 setMessage:[defaultRegisteredNum objectForKey:@"Number"]];
    [push1 setData:data];
    [push1 sendPushInBackground];
    
    
    //////////////////////////////////////////////////////////////////////////
    
    // Sending push via Netmera
    /*Commenting this because client DoesNt need Netmera Push
     
     NSArray *tags = @[receiver];
     NMPushSenderObject *pushObject = [[NMPushSenderObject alloc] initWithTitle:@"Suprcall" alertText:strAlert];
     [pushObject setTags:[tags mutableCopy]];
     pushObject.customParams = [NSMutableDictionary dictionaryWithObject:[defaultRegisteredNum objectForKey:@"Number"] forKey:@"Mesage"];
     //pushObject.customPayload = [NSMutableDictionary dictionaryWithObject:[defaultRegisteredNum objectForKey:@"Number"] forKey:@"Mesage"];
     [pushObject sendPushNotificationWithCompletionHandler:^(NSString *notificationId, NSError *error) {
     if (!error)
     NSLog(@"The push notification is sent successfully.");
     else
     NSLog(@"Error occurred. Reason: %@", error.localizedDescription);
     }];
     */
    //////////////////////////////////////////////////////////////////////////
    
    
    /*
     if (!allowToCallUrl) { // for a non suprcall user if sms is not sent, then no need to call the url;
     [HUD hide:YES];
     allowToCallUrl = YES;
     return;
     }
     */
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init] ;
    [df setDateFormat:@"HH:mm:ss"];
    
    NSDate *now = [NSDate date];
    backGroundDate=now;
    //NSString *date_time = [self formateDate:backGroundDate];
    backGroundDate = [self correctedFormat:backGroundDate];
    
    //NSLog(@"date_time = %@",date_time);
    CTTelephonyNetworkInfo *telephonyInfo = [CTTelephonyNetworkInfo new];
    NSLog(@"Current Radio Access Technology: %@", telephonyInfo.subscriberCellularProvider.carrierName);
    
    // NSDictionary *dictParms = [NSDictionary dictionaryWithObjectsAndKeys:phnNumToCall,@"dialledNum",phoneCode,@"receiverCountryCode",[NSString stringWithFormat:@"%f",value4Lat],@"Lat",[NSString stringWithFormat:@"%f",value4Long],@"Long",backGroundDate,@"startTime",telephonyInfo.subscriberCellularProvider.carrierName,@"gsmOperator", nil];
    
    if ([formatedPhoneNumber hasPrefix:@"+"]) {
        NSLog(@"Entered formatedPhoneNumber");
        NSString *strPhnWithoutPlus = [formatedPhoneNumber substringFromIndex:1];
        formatedPhoneNumber = [NSString stringWithFormat:@"~%@",strPhnWithoutPlus];
    }
    
    NSString *localURL=[[NSString alloc] initWithFormat:@"data=callDetails&userNum=%@&userCountryCode=%@&dialledNum=%@&receiverCountryCode=%@&Lat=%@&Long=%@&Duration=%@&startTime=%@&endTime=%@&callStatus=%@&gsmOperator=%@",[defaultRegisteredNum objectForKey:@"Number"],[defaultsCountryCode objectForKey:@"countryCode"],formatedPhoneNumber,phoneCode,[NSString stringWithFormat:@"%f",value4Lat],[NSString stringWithFormat:@"%f",value4Long],@"",backGroundDate,@"",@"push",telephonyInfo.subscriberCellularProvider.carrierName];
    
    localURL=[localURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"LOCAL Recent==%@",localURL);
    
    // [SendCallDetailsPost sendCallDetailsPostMethod:dictParms];
    
    [self getUrlAndParseForCallDetails:baseUrl :localURL];
    reloadingNeeded = YES;
}




-(void)getUrlAndParseForRegisteredData:(NSString *)url
{
    
    HUD = [[MBProgressHUD alloc]initWithView:self.view];
    if ([globalLanguage isEqualToString:@"tr"]) {
        HUD.labelText = @"profil gönderme...";
    }
    else
        HUD.labelText = @"Sending Profile...";
    HUD.mode = MBProgressHUDModeIndeterminate;
    [HUD show:YES];
    [self.view addSubview:HUD];
    
    NSURL *completeUrl=[NSURL URLWithString:url];
    NSLog(@"URL===%@",url);
    dispatch_async(kBackgroundQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL:completeUrl];
        [self performSelectorOnMainThread:@selector(fetchedDataForRegisteredData:) withObject:data waitUntilDone:YES];
    });
    
}

- (void)fetchedDataForRegisteredData:(NSData *)responseData
{
    if(responseData)
    {
        NSError* error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData //1
                                                             options:kNilOptions
                                                               error:&error];
        if (error) {
            
            if ([globalLanguage isEqualToString:@"tr"])
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"hata" message:@"SuprCall beklenmedik bir hata ile karşılaştı, lütfen internet bağlantınızı kontrol ediniz.." delegate:self cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            else
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"SuprCall encountered an unexpected error, please verify your wifi or internet connection is working properly." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            
            [HUD hide:YES];
            
            return;
        }
        else
        {
            //NSLog(@"TEST JSON===%@",json);
            
            NSMutableArray *arrOfRegisteredNum=[[NSMutableArray alloc]init];
            arrOfRegisteredNum=[json objectForKey:@"Registered Number"];
            
            NSString *strEnteredNum=strSelectedNumber;
            
            if ([strEnteredNum length]>10) {
                strEnteredNum=[strEnteredNum substringFromIndex:[strEnteredNum length]-10];
            }
            for (int i=0; i < [arrOfRegisteredNum count]; i++)
            {
                if ([strEnteredNum isEqualToString:[arrOfRegisteredNum objectAtIndex:i]]) {
                    isMatched=YES;
                    [self sendPush:[arrOfRegisteredNum objectAtIndex:i]];
                    break;
                }
            }
            if (!isMatched)
            {
                
                if ([globalLanguage isEqualToString:@"tr"]) {
                    HUD.labelText = @"çağrı...";
                }
                else
                    HUD.labelText = @"Calling...";
                
                
                NSString *strUrl;
                defaultsCountryCode = [NSUserDefaults standardUserDefaults];
                defaultRegisteredNum = [NSUserDefaults standardUserDefaults];
                
                /*SendSMSLink Which is not used commented due to this///////
                 NSDictionary *dictSms;
                 if ([globalLanguage isEqualToString:@"tr"])
                 {
                 dictSms = [NSDictionary dictionaryWithObjectsAndKeys:[self formatPhoneNumberForCall:strSelectedNumber],@"phnNum",[CustomClassForFormatString formatString:strGlobalUserName],@"userName",[defaultsCountryCode objectForKey:@"countryCode"],@"countryCode",@"ver",@"tr", nil];
                 
                 strUrl=[NSString stringWithFormat:@"%@point.php?data=sendMsg&phnNum=%@&userName=%@&countryCode=%@&userNum=%@&ver=tr",kBaseUrl,[self formatPhoneNumberForCall:strSelectedNumber],[CustomClassForFormatString formatString:strGlobalUserName],[defaultsCountryCode objectForKey:@"countryCode"],[defaultRegisteredNum objectForKey:@"Number"]];
                 }
                 else
                 {
                 strUrl=[NSString stringWithFormat:@"%@point.php?data=sendMsg&phnNum=%@&userName=%@&countryCode=%@&userNum=%@&ver=",kBaseUrl,[self formatPhoneNumberForCall:strSelectedNumber],[CustomClassForFormatString formatString:strGlobalUserName],[defaultsCountryCode objectForKey:@"countryCode"],[defaultRegisteredNum objectForKey:@"Number"]];
                 }
                 
                 strUrl=[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                 
                 [self getUrlAndParseForSendSms:strUrl];
                 
                 */
                
            }
            //NSLog(@"TTTTTT===%@",strEnteredNum);
            
            /*for (int i=0; i < [arrOfRegisteredNum count]; i++)
             {
             NSString *strTemp=[arrOfRegisteredNum objectAtIndex:i];
             NSLog(@"STTT===%@",strTemp);
             strEnteredNum=[self formatPhoneNumber:strEnteredNum];
             if ([strEnteredNum rangeOfString:strTemp].location!=NSNotFound)
             {
             
             NSDateComponents *comps = [[NSDateComponents alloc] init];
             [comps setYear:2013];
             [comps setMonth:8];
             [comps setDay:6];
             NSCalendar *gregorian =
             [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
             NSDate *date = [gregorian dateFromComponents:comps];
             
             defaultRegisteredNum = [NSUserDefaults standardUserDefaults];
             NSString *str;
             if ([strTemp isEqualToString:[defaultRegisteredNum objectForKey:@"Number"]])
             {
             str=@"";
             }
             else
             str=[NSString stringWithFormat:@"%@%@",@"a",strTemp];
             
             PFQuery *pushQuery = [PFInstallation query];
             [pushQuery whereKey:@"channels" equalTo:str];
             
             defaultRegisteredNum = [NSUserDefaults standardUserDefaults];
             NSString *strPhoneNum=[defaultRegisteredNum objectForKey:@"Number"];
             
             NSString *strAlert;
             if ([globalLanguage isEqualToString:@"tr"])
             {
             
             if ([[NSString stringWithFormat:@"%@",[dictionaryUserDetails objectForKey:@"Location Option"]] isEqualToString:@"NO"]||[[NSString stringWithFormat:@"%@",[dictionaryUserDetails objectForKey:@"Location Option"]] isEqualToString:@"no"])
             {
             strAlert=[NSString stringWithFormat:@"%@ %@ az sonra sizi arayabilir!",[CustomClassForFormatString formatString:[dictionaryUserDetails objectForKey:@"First Name"]],[CustomClassForFormatString formatString:[dictionaryUserDetails objectForKey:@"Last Name"]]];
             }
             else
             {
             strAlert=[NSString stringWithFormat:@"%@ %@,%@ az sonra sizi arayabilir!",[CustomClassForFormatString formatString:[dictionaryUserDetails objectForKey:@"First Name"]],[CustomClassForFormatString formatString:[dictionaryUserDetails objectForKey:@"Last Name"]],[CustomClassForFormatString formatString:[dictionaryUserDetails objectForKey:@"Location"]]];
             }
             
             }
             else
             {
             if ([[NSString stringWithFormat:@"%@",[dictionaryUserDetails objectForKey:@"Location Option"]] isEqualToString:@"NO"]||[[NSString stringWithFormat:@"%@",[dictionaryUserDetails objectForKey:@"Location Option"]] isEqualToString:@"no"])
             {
             strAlert=[NSString stringWithFormat:@"%@ %@ may call you soon !",[CustomClassForFormatString formatString:[dictionaryUserDetails objectForKey:@"First Name"]],[CustomClassForFormatString formatString:[dictionaryUserDetails objectForKey:@"Last Name"]]];
             }
             else
             {
             strAlert=[NSString stringWithFormat:@"%@ %@,%@ may call you soon !",[CustomClassForFormatString formatString:[dictionaryUserDetails objectForKey:@"First Name"]],[CustomClassForFormatString formatString:[dictionaryUserDetails objectForKey:@"Last Name"]],[CustomClassForFormatString formatString:[dictionaryUserDetails objectForKey:@"Location"]]];
             }
             
             }
             
             NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
             strAlert, @"alert",
             strPhoneNum, @"Mesage",
             @"Increment", @"badge",
             @"cheering.caf", @"sound",
             nil];
             
             PFPush *push1 = [[PFPush alloc] init];
             [push1 setQuery:pushQuery]; // Set our Installation query
             [push1 expireAtDate:date];
             [push1 setQuery:pushQuery];
             [push1 setMessage:strPhoneNum];
             [push1 setData:data];
             [push1 sendPushInBackground];
             
             defaultsCountryCode = [NSUserDefaults standardUserDefaults];
             NSString *strPhoneNumber=[self formatPhoneNumberForCall:strSelectedNumber];
             [HUD setHidden:YES];*/
            
            /*NSString *firstLetter = [strPhoneNumber substringToIndex:1];
             if ([firstLetter isEqualToString:@"+"]) {
             strPhoneNumber=[self formatPhoneNumberForCall:strPhoneNumber];
             }*/
            /* NSDateFormatter *df = [[NSDateFormatter alloc] init] ;
             [df setDateFormat:@"HH:mm:ss"];
             NSDate *now = [NSDate date];
             backGroundDate=now;
             
             NSString *localURL=[[NSString alloc] initWithFormat:@"data=callDetails&userNum=%@&userCountryCode=%@&dialledNum=%@&Lat=%@&Long=%@&Duration=%@&startTime=%@&endTime=%@&callStatus=%@",[defaultRegisteredNum objectForKey:@"Number"],[defaultsCountryCode objectForKey:@"countryCode"],strPhoneNumber,[NSString stringWithFormat:@"%f",value4Lat],[NSString stringWithFormat:@"%f",value4Long],@"",backGroundDate,@"",@"push"];
             
             localURL=[localURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
             [self getUrlAndParseForCallDetails:baseUrl :localURL];
             
             break;*/
            
        }
        
    }
}

-(void)sendPush:(NSString *)toNum
{
    
    
    NSString *strTemp=toNum;
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:2013];
    [comps setMonth:8];
    [comps setDay:6];
    //    NSCalendar *gregorian =
    //    [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    //NSDate *date = [gregorian dateFromComponents:comps];
    
    defaultRegisteredNum = [NSUserDefaults standardUserDefaults];
    NSString *str;
    if ([strTemp isEqualToString:[defaultRegisteredNum objectForKey:@"Number"]])
    {
        str=@"";
    }
    else if([phoneCode isEqualToString:@"375"])
    {
        if([strTemp hasPrefix:@"80"] && ![strTemp hasPrefix:@"800"])
        {
            str = [NSString stringWithFormat:@"a375%@",[strTemp substringFromIndex:2]];
        }
    }
    
    else
        str=[NSString stringWithFormat:@"%@%@",@"a",strTemp];
    
    if(str.length < 7)
    {
        str = @"12345678";
    }
    
    
    PFQuery *pushQuery = [PFInstallation query];
    [pushQuery whereKey:@"channels" equalTo:str];
    
    defaultRegisteredNum = [NSUserDefaults standardUserDefaults];
    NSString *strPhoneNum=[defaultRegisteredNum objectForKey:@"Number"];
    
    NSString *strAlert;
    
    NSString *myName = [NSString stringWithFormat:@"%@ %@",[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"fname"]],[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"lname"]]];
    
    if ([globalLanguage isEqualToString:@"tr"])
    {
        if(myName.length < 2)
        {
            strAlert = @"Az sonra SuprCall ile aranabilirsiniz.";
        }
        else
        {
            if ([[[NSUserDefaults standardUserDefaults]stringForKey:@"location"] isEqualToString:@"(null),(null)"] || ![[NSUserDefaults standardUserDefaults]boolForKey:@"Checkbox4"])
            {
                strAlert=[NSString stringWithFormat:@"%@ %@, az sonra sizi arayabilir!",[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"fname"]],[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"lname"]]];
            }
            else
            {
                strAlert=[NSString stringWithFormat:@"%@ %@, %@, az sonra sizi arayabilir!",[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"fname"]],[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"lname"]],[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"location"]]];
            }
        }
        
    }
    else
    {
        
        if(myName.length < 2)
        {
            strAlert = @"You may recieve a SuprCall soon.";
        }
        else
        {
            if ([[[NSUserDefaults standardUserDefaults]stringForKey:@"location"] isEqualToString:@"(null),(null)"] || ![[NSUserDefaults standardUserDefaults]boolForKey:@"Checkbox4"])
            {
                strAlert=[NSString stringWithFormat:@"%@ %@, may call you soon !",[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"fname"]],[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"lname"]]];
            }
            else
            {
                strAlert=[NSString stringWithFormat:@"%@ %@, %@, may call you soon !",[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"fname"]],[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"lname"]],[CustomClassForFormatString formatString:[[NSUserDefaults standardUserDefaults]stringForKey:@"location"]]];
            }
        }
    }
    
    strAlert = [strAlert stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    
    if([strAlert isEqualToString:@" , , may call you soon !"])
    {
        strAlert = @"You may recieve a SuprCall soon.";
    }
    else if ([strAlert isEqualToString:@" , , az sonra sizi arayabilir!"])
    {
        strAlert = @"Az sonra SuprCall ile aranabilirsiniz.";
    }
    
    //////////////////////////////////////////////////////////////////////////
    
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          strAlert, @"alert",
                          strPhoneNum, @"Mesage",
                          phoneCode,@"country_code",
                          @"Increment", @"badge",
                          @"cheering.caf", @"sound",
                          @"superAction",@"action",
                          nil];
    
    PFPush *push1 = [[PFPush alloc] init];
    [push1 setQuery:pushQuery]; // Set our Installation query
    //[push1 expireAtDate:date];
    [push1 expireAfterTimeInterval:60];
    [push1 setQuery:pushQuery];
    [push1 setMessage:strPhoneNum];
    [push1 setData:data];
    [push1 sendPushInBackground];
    
    
    // Sending push via Netmera
    /*Commenting this because client DoesNt need Netmera Push
     
     NSArray *tags = @[str];
     NMPushSenderObject *pushObject = [[NMPushSenderObject alloc] initWithTitle:@"Suprcall" alertText:strAlert];
     [pushObject setTags:[tags mutableCopy]];
     pushObject.customParams = [NSMutableDictionary dictionaryWithObject:[defaultRegisteredNum objectForKey:@"Number"] forKey:@"Mesage"];
     //pushObject.customPayload = [NSMutableDictionary dictionaryWithObject:[defaultRegisteredNum objectForKey:@"Number"] forKey:@"Mesage"];
     [pushObject sendPushNotificationWithCompletionHandler:^(NSString *notificationId, NSError *error) {
     if (!error)
     NSLog(@"The push notification is sent successfully.");
     else
     NSLog(@"Error occurred. Reason: %@", error.localizedDescription);
     }];
     */
    //////////////////////////////////////////////////////////////////////////
    
    
    //[HUD setHidden:YES];
    defaultsCountryCode = [NSUserDefaults standardUserDefaults];
    // NSString *strPhoneNumber=self.lblPhoneScreen.text;
    NSString *strPhoneNumber=[self formatPhoneNumberForCall:strSelectedNumber];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init] ;
    [df setDateFormat:@"HH:mm:ss"];
    NSDate *now = [NSDate date];
    backGroundDate=now;
    // NSString *date_time = [self formateDate:backGroundDate];
    backGroundDate=[self correctedFormat:backGroundDate];
    
    // NSLog(@"date_time = %@",date_time);
    
    if(strPhoneNumber.length < 6)
    {
        return;
    }
    
    CTTelephonyNetworkInfo *telephonyInfo = [CTTelephonyNetworkInfo new];
    NSLog(@"Current Radio Access Technology: %@", telephonyInfo.subscriberCellularProvider.carrierName);
    
    if ([strPhoneNumber hasPrefix:@"+"]) {
        NSLog(@"Entered formatedPhoneNumber");
        NSString *strPhnWithoutPlus = [strPhoneNumber substringFromIndex:1];
        strPhoneNumber = [NSString stringWithFormat:@"~%@",strPhnWithoutPlus];
    }
    
    
    NSString *localURL=[[NSString alloc] initWithFormat:@"data=callDetails&userNum=%@&userCountryCode=%@&dialledNum=%@&receiverCountryCode=%@&Lat=%@&Long=%@&Duration=%@&startTime=%@&endTime=%@&callStatus=%@&gsmOperator=%@",[defaultRegisteredNum objectForKey:@"Number"],[defaultsCountryCode objectForKey:@"countryCode"],strPhoneNumber,phoneCode,[NSString stringWithFormat:@"%f",value4Lat],[NSString stringWithFormat:@"%f",value4Long],@"",backGroundDate,@"",@"push",telephonyInfo.subscriberCellularProvider.carrierName];
    
    phnNumToCall = strPhoneNum;
    
    localURL=[localURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self getUrlAndParseForCallDetails:baseUrl :localURL];
    reloadingNeeded = YES;
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kAlertText)
    {
        if(buttonIndex)
        {
            ADManager *manager = [ADManager sharedManager];
            if(manager.jsonDict)
            {
                NSArray *adArray = manager.jsonDict[@"Ads"];
                SCAdViewController *adViewController = [[SCAdViewController alloc]initWithNibName:@"SCAdViewController" bundle:nil];
                adViewController.dict = [adArray firstObject];
                adViewController.phoneCallUrl = phoneURL;
                [self.navigationController presentViewController:adViewController animated:YES completion:nil];
            }
            else
            {
                [self performSelector:@selector(makeCallWithUrl:) withObject:phoneURL afterDelay:3];
            }
        }
        else
        {
            [self performSelector:@selector(makeCallWithUrl:) withObject:phoneURL afterDelay:3];
        }
    }
}

-(void)callPhone:(NSString*)contacts
{
    NSLog(@"phone number = %@",phnNumToCall);
    NSString *phoneURLString = [NSString stringWithFormat:@"telprompt:%@", phnNumToCall];
    phoneURL = [NSURL URLWithString:phoneURLString];
    
    if(switchIsOn)
    {
        // [self showAdAccordingly];
        
        // isShowAd = NO;
        [self presentingAd];
        
        /*
         IMAdViewController *imaView = [[IMAdViewController alloc]initWithNibName:@"IMAdViewController" bundle:nil];
         imaView.phoneCallUrl = phoneURL;
         [self presentViewController:imaView animated:YES completion:nil];
         
         ////////////old/////////////////////////
         ADManager *manager = [ADManager sharedManager];
         if(manager.jsonDict)
         {
         NSArray *adArray = manager.jsonDict[@"Ads"];
         SCAdViewController *adViewController = [[SCAdViewController alloc]initWithNibName:@"SCAdViewController" bundle:nil];
         adViewController.dict = [adArray firstObject];
         adViewController.phoneCallUrl = phoneURL;
         [self.navigationController presentViewController:adViewController animated:YES completion:nil];
         }
         else
         {
         [self performSelector:@selector(makeCallWithUrl:) withObject:phoneURL afterDelay:3];
         }
         */
        /*
         if([[ADManager sharedManager]arrayText])
         {
         if ([globalLanguage isEqualToString:@"tr"])
         {
         
         UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:[[[ADManager sharedManager]arrayText] lastObject] delegate:self cancelButtonTitle:@"İptal" otherButtonTitles:@"Tamam", nil];
         alertview.tag = kAlertText;
         [alertview show];
         
         }
         else
         {
         UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:nil message:[[[ADManager sharedManager]arrayText] firstObject] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
         alertview.tag = kAlertText;
         [alertview show];
         }
         }
         */
    }
    else
    {
        [self performSelector:@selector(makeCallWithUrl:) withObject:phoneURL afterDelay:3];
    }
}

-(void)adManagerService{
    ADManager *manager = [ADManager sharedManager];
    if(manager.jsonDict)
    {
        NSArray *adArray = manager.jsonDict[@"Ads"];
        SCAdViewController *adViewController = [[SCAdViewController alloc]initWithNibName:@"SCAdViewController" bundle:nil];
        adViewController.dict = [adArray firstObject];
        adViewController.phoneCallUrl = phoneURL;
        [self.navigationController presentViewController:adViewController animated:YES completion:nil];
    }
    else
    {
        [self performSelector:@selector(makeCallWithUrl:) withObject:phoneURL afterDelay:3];
    }
}

-(void)showAdAccordingly{
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"GADAd"]) {
        [self createAndLoadGADInterstitial];
    }else if([[NSUserDefaults standardUserDefaults]boolForKey:@"DFBAd"]){
        [self createAndLoadDFBInterstitial];
    }else{
        [self adManagerService];
    }
}

-(void)loadAdIfNeeded{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"GADAd"]) {
        interstitialGAD.isReady ? nil: [self showAdAccordingly];
    }else if([[NSUserDefaults standardUserDefaults]boolForKey:@"DFBAd"]){
        interstitialDFP.isReady ? nil :[self showAdAccordingly];
    }else{
        // [self adManagerService];
    }
    
}

-(void)presentingAd{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"GADAd"]) {
        interstitialGAD.isReady ? [interstitialGAD presentFromRootViewController:self]: [self makeCallWithUrl:phoneURL];
    }else if([[NSUserDefaults standardUserDefaults]boolForKey:@"DFBAd"]){
        interstitialDFP.isReady ? [interstitialDFP presentFromRootViewController:self] :[self makeCallWithUrl:phoneURL];
    }else{
        // [self adManagerService];
    }
}

-(void)createAndLoadGADInterstitial{
    interstitialGAD = [ADMobCustomClass currentGADInterstitial];
    interstitialGAD.delegate = self;
    // [self performSelector:@selector(makeInterstitial) withObject:nil afterDelay:2];
}

-(void)createAndLoadDFBInterstitial{
    interstitialDFP = [ADMobCustomClass currentDFBInterstitial];
    interstitialDFP.delegate = self;
    //[self performSelector:@selector(makeInterstitial) withObject:nil afterDelay:2];
}


//http://www.suprcall.com/suprcall/point.php?data=admobhit&usernum=5332101234&countrycode=90&countryphone=905332101234&adid=352
- (void)sentAdStatusToServer:(NSString *)adid
{
    NSString *myNumber = [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"],[[NSUserDefaults standardUserDefaults]objectForKey:@"Number"]];
    
    NSString *countryCode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"countryCode"]];
    NSString *myPhoneNumbr = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"Number"]];
    
    NSMutableDictionary *parms = [[NSMutableDictionary alloc]init];
    [parms setObject:countryCode forKey:@"countrycode"];
    [parms setObject:myNumber forKey:@"countryphone"];
    [parms setObject:@"admobhit" forKey:@"data"];
    [parms setObject:myPhoneNumbr forKey:@"usernum"];
    [parms setObject:adid forKey:@"adid"];
    
    // NSLog(@"dict = %@",parms);
    /*
     [[Parser sharedParser]parseLinkByAppendingString:@"point.php?" withJsonKey:nil WithParms:parms Success:^(id success) {
     NSLog(@"success... %@",success);
     } Failure:^(NSError *error) {
     }];
     */
    
    [[Parser sharedParser]parseLinkByPostMethodAppendingString:@"point.php?" withJsonKey:nil WithParms:parms Success:^(id success) {
        NSLog(@"successPost... %@",success);
        
    } Failure:^(NSError *error) {
        NSLog(@"error = %@",error.localizedDescription);
    }];
}

- (void)makeCallWithUrl:(NSURL *)url
{
    NSLog(@" phone url = %@",url);
    // if (!isAlertShown) {
    //     isAlertShown = YES;
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
        // [self performSelector:@selector(changeAlertMode) withObject:nil afterDelay:1.0];
    }
    //  }
}

-(void)changeAlertMode{
    isAlertShown = NO;
}
-(void)getUrlAndParseForUSERData:(NSString *)url
{
    NSURL *completeUrl=[NSURL URLWithString:url];
    NSLog(@"URL===%@",url);
    dispatch_async(kBackgroundQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL:completeUrl];
        [self performSelectorOnMainThread:@selector(fetchedDataForUser:) withObject:data waitUntilDone:YES];
    });
}

- (void)fetchedDataForUser:(NSData *)responseData
{
    if(responseData)
    {
        NSError* error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData //1
                                                             options:kNilOptions
                                                               error:&error];
        if (error) {
            
            if ([globalLanguage isEqualToString:@"tr"])
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"hata" message:@"SuprCall beklenmedik bir hata ile karşılaştı, lütfen internet bağlantınızı kontrol ediniz.." delegate:self cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            else
            {
                UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"SuprCall encountered an unexpected error, please verify your wifi or internet connection is working properly." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [noConnectionAlert show];
            }
            
            [HUD hide:YES];
            
            return;
        }
        else
        {
            //NSLog(@"TEST JSON===%@",json);
            NSDictionary *dicTemp=[json objectForKey:@"Viewer Details"];
            
            NSLog(@"TEST JSON===%@",dicTemp);
            
            if ([dicTemp count]==0)
            {
                [HUD hide:YES];
                if ([globalLanguage isEqualToString:@"tr"])
                {
                    UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"SuprCall" message:@"Seçilen kişi SuprCall kullanmadığı için profil ayrıntıları görüntülenemiyor." delegate:self cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
                    [noConnectionAlert show];
                }
                else
                {
                    UIAlertView *noConnectionAlert = [[UIAlertView alloc] initWithTitle:@"SuprCall" message:@"The selected contact is not a SuprCall user. The contact’s profile details can not be displayed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [noConnectionAlert show];
                }
            }
            else
            {
                if([[UIScreen mainScreen]bounds].size.height == 568)
                {
                    if ([globalLanguage isEqualToString:@"tr"])
                    {
                        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_Turk" bundle: nil];
                        ContactDetailsVC *controller = (ContactDetailsVC *)[mainStoryboard instantiateViewControllerWithIdentifier:@"ContactDetailsVC"];
                        controller.dic=dicTemp;
                        controller.strFromClass=@"Recent";
                        controller.strSelectedPhnNum=strSelected;
                        [self.navigationController pushViewController:controller animated:YES];
                        
                    }
                    else
                    {
                        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle: nil];
                        ContactDetailsVC *controller = (ContactDetailsVC *)[mainStoryboard instantiateViewControllerWithIdentifier:@"ContactDetailsVC"];
                        controller.dic=dicTemp;
                        controller.strFromClass=@"Recent";
                        controller.strSelectedPhnNum=strSelected;
                        [self.navigationController pushViewController:controller animated:YES];
                    }
                }
                else
                {
                    if ([globalLanguage isEqualToString:@"tr"])
                    {
                        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone4_Turk" bundle: nil];
                        ContactDetailsVC *controller = (ContactDetailsVC *)[mainStoryboard instantiateViewControllerWithIdentifier:@"ContactDetailsVC"];
                        controller.dic=dicTemp;
                        controller.strFromClass=@"Recent";
                        controller.strSelectedPhnNum=strSelected;
                        [self.navigationController pushViewController:controller animated:YES];
                    }
                    else
                    {
                        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone4" bundle: nil];
                        ContactDetailsVC *controller = (ContactDetailsVC *)[mainStoryboard instantiateViewControllerWithIdentifier:@"ContactDetailsVC"];
                        controller.dic=dicTemp;
                        controller.strFromClass=@"Recent";
                        controller.strSelectedPhnNum=strSelected;
                        [self.navigationController pushViewController:controller animated:YES];
                    }
                }
            }
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    for (UIView *alrtV in [self.view subviews]) {
        if ([alrtV isKindOfClass:[MyAlertView class]]) {
            [alrtV removeFromSuperview];
        }
    }
    
    if(HUD.hidden == NO)
    {
        [HUD hide:YES];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ReceivedPush" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CTCallStateDidChangeStarted" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CTCallStateDidChangeEnded" object:nil];
    
}

- (void)viewDidUnload {
    [self setTblRecentCall:nil];
    [super viewDidUnload];
}

-(NSString *)formatPhoneNumber:(NSString *)strPhoneNumber
{
    NSString *strTemp=nil;
    NSCharacterSet *charSet=[NSCharacterSet characterSetWithCharactersInString:@"!()$%@%&*_+|-#"];
    strTemp=[strPhoneNumber stringByTrimmingCharactersInSet:charSet];
    strTemp=[strTemp stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [strTemp length])];
    
    strTemp=[strTemp stringByReplacingOccurrencesOfString:@"(" withString:@""];
    strTemp=[strTemp stringByReplacingOccurrencesOfString:@")" withString:@""];
    strTemp=[strTemp stringByReplacingOccurrencesOfString:@"-" withString:@""];
    strTemp=[strTemp stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //NSLog(@"STRTEMP===%@",strTemp);
    return strTemp;
}

-(NSString *)formatPhoneNumberForCall:(NSString *)strPhoneNumber
{
    NSString *strTemp=nil;
    NSCharacterSet *charSet=[NSCharacterSet characterSetWithCharactersInString:@"!()$%@%&*_|-#"];
    strTemp=[strPhoneNumber stringByTrimmingCharactersInSet:charSet];
    
    
    strTemp=[strTemp stringByReplacingOccurrencesOfString:@"(" withString:@""];
    strTemp=[strTemp stringByReplacingOccurrencesOfString:@")" withString:@""];
    strTemp=[strTemp stringByReplacingOccurrencesOfString:@"-" withString:@""];
    strTemp=[strTemp stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //NSLog(@"STRTEMP===%@",strTemp);
    return strTemp;
}

- (IBAction)btnOutGoing:(id)sender {
    strStatus=@"out";
    [self.tblRecentCall reloadData];
}

- (IBAction)btnIncoming:(id)sender {
    strStatus=@"in";
    [self.tblRecentCall reloadData];
}

-(NSString *)formateDate:(NSDate *)myDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    // NSLog(@"%@", [formatter stringFromDate:myDate]);
    return [formatter stringFromDate:myDate];
}

- (NSDate *)correctedFormat:(NSDate*)myDate
{
    NSDate* sourceDate = myDate;
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *newDate = [sourceDate dateByAddingTimeInterval:interval];
    return newDate;
}

#pragma mark GADInterstitialDelegate implementation

- (void)interstitialDidReceiveAd:(GADInterstitial *)interstitials {
    
    [self sentAdStatusToServer:interstitials.adUnitID];
}

- (void)interstitial:(DFPInterstitial *)interstitials didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"interstitialDidFailToReceiveAdWithError: %@", [error localizedDescription]);
    [self makeCallWithUrl:phoneURL];
    // [self showAdAccordingly];
    
    // self.view.userInteractionEnabled = YES;
}

- (void)interstitialDidDismissScreen:(DFPInterstitial *)interstitial {
    NSLog(@"interstitialDidDismissScreen");
    [self makeCallWithUrl:phoneURL];
    //isShowAd ? nil : [self makeCallWithUrl:phoneURL];
    //isShowAd = NO;
    [self showAdAccordingly];
    // self.view.userInteractionEnabled = YES;
    
}

@end
