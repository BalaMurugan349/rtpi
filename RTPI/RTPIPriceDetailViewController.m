//
//  RTPIPriceDetailViewController.m
//  RTPI
//
//  Created by apple on 11/6/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//


#import "RTPIPriceDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "RTPIWebServiceManager.h"
#import "RTPIUser.h"
#import "MBProgressHUD.h"
#import "RTPIContracts.h"
#import "RTPIAppDelegate.h"

@interface RTPIPriceDetailViewController ()<UITableViewDataSource,UITableViewDelegate,RTPIWebserviceDelegateForPricePage>
{
    NSArray *leftLabelArray;
      NSArray *rightLabelArray;
    MBProgressHUD *HUD;
    UIRefreshControl *refreshControl;
}
@property (weak, nonatomic) IBOutlet UISegmentedControl *segControl;
- (IBAction)segControlAction:(UISegmentedControl *)sender;

@end

@implementation RTPIPriceDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        leftLabelArray = @[@"Open",@"High",@"Low",@"Old Settlement"];
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [self refreshPage];
  
   RTPIAppDelegate *delegate = [UIApplication sharedApplication].delegate;
   delegate.isEnteredGraph = YES;
   
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)  name:UIDeviceOrientationDidChangeNotification  object:nil];
    [self supportedInterfaceOrientations];
     [[UINavigationBar appearance]setBackgroundColor:[UIColor colorWithRed:19.0f/255.0f green:75.0f/255.0f blue:31.0f/255 alpha:1] ] ;

    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:Nil action:nil];
    fixedSpace.width = 35.0f;
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 setFrame: CGRectMake(0, 0, 17, 17)];
    button2.titleLabel.font = [UIFont systemFontOfSize:13.0f] ;
    [button2 setBackgroundColor:[UIColor clearColor]];
     [button2 setTitle:@"Graph" forState:UIControlStateNormal];
    [button2 setImage:[UIImage imageNamed:@"graph.jpeg"] forState:UIControlStateNormal];
    [button2 setImage:[UIImage imageNamed:@"graph.jpeg"] forState:UIControlStateHighlighted];
    [self.navigationItem setTitle:_dicData[@"type"]];
 }

-(void)backbuttonPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Customise Navigation Bar
-(void)customiseNavigationBar{
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:18]};
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
      UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 57, 25)];
   
    [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(backbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *buttn = [[UIBarButtonItem alloc]initWithCustomView:backButton];
     [backButton setTitle:@"Back" forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = buttn;
    [self customiseNavigationBar];
    self.title = self.selectedContracts.contractMonth;
    rightLabelArray=@[_dicData[@"Open"],_dicData[@"high"],_dicData[@"low"],_dicData[@"Old_Settlement"]];
}
-(void)refreshInvoked{
    [refreshControl endRefreshing];
}
#pragma mark - TableViewDelegate And DataSources

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return leftLabelArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *priceDetailCell;
    static NSString *cellIdentifier = @"PriceDetailCell";
    priceDetailCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!priceDetailCell) {
        priceDetailCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        priceDetailCell = [[[NSBundle mainBundle]loadNibNamed:@"PriceDetailCell" owner:self options:nil]objectAtIndex:0];
    }
    UILabel *leftLabelForCurntIndexPath = (UILabel *) [priceDetailCell viewWithTag:1];
    [leftLabelForCurntIndexPath setText:[leftLabelArray objectAtIndex:indexPath.row]];
    
     UILabel *rightLabelForCurntIndexPath = (UILabel *) [priceDetailCell viewWithTag:2];
    float value=[[rightLabelArray objectAtIndex:indexPath.row]floatValue];
    [rightLabelForCurntIndexPath setText:[NSString stringWithFormat:@"%.2f",value]];
    [priceDetailCell setBackgroundColor:[UIColor clearColor]];
    return priceDetailCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
   // self.headerView.layer.borderWidth = 1.0f;
    
    self.headerView.backgroundColor = [UIColor colorWithRed:19.0f/255.0f green:75.0f/255.0f blue:31.0f/255 alpha:1]  ;
    [self populateTableHeader];
    return self.headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 61;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45.0f;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)populateTableHeader{
    self.contractName.text =_dicData[@"f_month"];// self.selectedContracts.typeName;
    self.contrctValue.text =[NSString stringWithFormat:@"%.2f",[_dicData[@"last"]floatValue]];// self.selectedContracts.last ;
  //  self.changeLabel.text  = [NSString stringWithFormat:@"%.2f",_selectedContracts.change.floatValue];
    float change=[_dicData[@"net"]floatValue];
    self.changeLabel.text=[NSString stringWithFormat:@"%.3f",change];
    NSString *str=@"%";
    float percentage=[_dicData[@"PerChange"]floatValue];
   // self.changeLabel.text = @"0.000";
    if([self.changeLabel.text isEqualToString:@"0.000"]){
    self.imageView.image = [UIImage imageNamed:@"equalArrow"];
        self.imageView.contentMode =UIViewContentModeScaleAspectFit;
//        self.changeLabel.textColor = [UIColor colorWithRed:229.0f/255.0f green:218.0f/255.0f blue:17.0f/255.0f alpha:1];
        self.changeLabel.textColor = [UIColor blackColor];

    }

    else if (change > 0) {
        self.changePercentLabel.textColor = [UIColor greenColor];
        self.changeLabel.textColor = [UIColor greenColor];
        self.changePercentLabel.text = [NSString stringWithFormat:@"+%.2f%@",percentage,str];
        self.imageView.image = [UIImage imageNamed:@"upArow.png"];
    }
    else
    {
         self.imageView.image = [UIImage imageNamed:@"downarrow.png"];
        self.changePercentLabel.textColor = [UIColor redColor];
        self.changePercentLabel.text = [NSString stringWithFormat:@"-%.2f%@",percentage,str];
        self.changeLabel.textColor = [UIColor redColor];
    }
    self.dateLabel.text = _stringDate;
}

-(void)refreshPage{
    
    NSString *url = [NSString stringWithFormat:@"/RefreshContractNew.aspx?id=%@&Position=0&Count=1", self.typeId];
    [HUD show:YES];
    RTPIWebServiceManager *webServiceManager = [[RTPIWebServiceManager alloc]initWithUrl:url andId:3];
    [webServiceManager setWebServicedelegateForPrice:self];
    
    
}

#pragma mark - WebserviceDelegates

-(void)didFinishParsingWithResultForPrice:(id)result{
    [HUD hide:YES];
    NSArray *array = (NSArray *)result;
    if (array.count >0) {
       
        self.selectedContracts = (RTPIContracts *)[ result objectAtIndex:0];
    //    rightLabelArray = @[self.selectedContracts.open,self.selectedContracts.high,self.selectedContracts.low];
        
        [self.detailsTableView reloadData];
    }
   
    [self populateTableHeader];

    
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)orientationChanged:(NSNotification *)notification{

//    UIInterfaceOrientation orientation = [[UIDevice currentDevice] orientation];
//    if (orientation == UIDeviceOrientationLandscapeLeft||orientation == UIDeviceOrientationLandscapeRight ) {
//    [self changeOrientation];
//    }
    
}
-(BOOL)shouldAutorotate
{
    return NO;
}
-(void)changeOrientation
{
    /*    //----
    CPDBarGraphViewController *graph = [[CPDBarGraphViewController alloc]init];
    graph.hidesBottomBarWhenPushed = YES;
    graph.monthContract = self.selectedContracts.contractMonth;
    graph.contractType  = self.selectedContracts.typeName;
    graph.date=self.selectedContracts.date;
    //----*/
    
     [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [_segControl setSelectedSegmentIndex:0];
[[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}
- (IBAction)segControlAction:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case 0:
            break;
            case 1:
            break;
        default:
            break;
    }
    
}
@end
