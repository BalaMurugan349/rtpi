//
//  RTPISelectePriceDetailViewController.h
//  RTPI
//
//  Created by SRISHTI INNOVATIVE on 07/06/14.
//  Copyright (c) 2014 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTPISelectePriceDetailViewController : UIViewController
@property(nonatomic,retain)NSArray *arrayPriceDetails;
@property(nonatomic,retain)NSString *type;
@end
