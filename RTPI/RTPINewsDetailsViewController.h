//
//  RTPINewsDetailsViewController.h
//  RTPI
//
//  Created by apple on 11/6/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTPINewsDetailsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *htmlString;

@property (strong, nonatomic) IBOutlet NSString *titleText;
@property (strong, nonatomic) IBOutlet NSString *dateText;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;


@end
