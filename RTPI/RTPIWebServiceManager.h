//
//  RTPIWebServiceManager.h
//  RTPI
//
//  Created by apple on 11/7/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol RTPIWebserviceDelegate <NSObject>
-(void)didFinishParsingWithResult:(id)result;
@end

@protocol RTPIWebserviceDelegateForPricePage <NSObject>
-(void)didFinishParsingWithResultForPrice:(id)result;
@end


@interface RTPIWebServiceManager : NSObject<NSURLConnectionDelegate>{
    NSMutableData *urlData;
    int urlIdntifier;
    NSMutableArray *rtpiContractDataModelarray;
}

-(id)initWithUrl:(NSString *)url andId:(int)identifier;
@property (nonatomic,strong) id<RTPIWebserviceDelegate> webServiceDelegate;
@property (nonatomic,strong) id<RTPIWebserviceDelegateForPricePage> webServicedelegateForPrice;

@end
