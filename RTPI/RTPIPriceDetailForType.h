//
//  RTPIPriceDetailForType.h
//  RTPI
//
//  Created by SRISHTI INNOVATIVE on 10/06/14.
//  Copyright (c) 2014 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTPIPriceDetailForType : NSObject
@property(nonatomic,retain)NSString *Id;
@property(nonatomic,retain)NSString *Contract_id;
@property(nonatomic,retain)NSString *type;
@property(nonatomic,retain)NSString *f_month;
@property(nonatomic,retain)NSString *last;
@property(nonatomic,retain)NSString *net;
@property(nonatomic,retain)NSString *PerChange;
@property(nonatomic,retain)NSString *Open;
@property(nonatomic,retain)NSString *high;
@property(nonatomic,retain)NSString *low;
@property(nonatomic,retain)NSString *Old_Settlement;
@property(nonatomic,retain)NSString *date;
-(id)initWithDictionary:(NSDictionary *)dicData;
-(id)initWithArray:(NSArray *)arrayData;
@end
