//
//  RTPISelectePriceDetailViewController.m
//  RTPI
//
//  Created by SRISHTI INNOVATIVE on 07/06/14.
//  Copyright (c) 2014 Abhishek. All rights reserved.
//

#import "RTPISelectePriceDetailViewController.h"
#import "ALUser.h"
#import "RTPIPriceDetailViewController.h"
@interface RTPISelectePriceDetailViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation RTPISelectePriceDetailViewController
{
    NSArray *mntharray;
    NSTimer *timer;
    ALUser *user;
    NSArray *arrayPriceDetailObjects;
    NSMutableArray *arrayDates;
    UIActivityIndicatorView *activityIndicator;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    mntharray = @[@"Jan",@"Feb",@"Mar",@"Apr",@"May",@"June",@"July",@"Aug",@"Sept",@"Oct",@"Nov",@"Dec"];
    [[UINavigationBar appearance]setBackgroundColor:[UIColor colorWithRed:19.0f/255.0f green:75.0f/255.0f blue:31.0f/255 alpha:1] ] ;
    [self.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgiphon4.png"]]];
    [self.navigationItem setTitle:_type];
    arrayDates=[NSMutableArray new];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 57, 25)];
    [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(backbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *buttn = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    
    activityIndicator = [[UIActivityIndicatorView alloc]
                         initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhite];
    // I've tried Gray, White, and WhiteLarge
    activityIndicator.hidden = NO;
    UIBarButtonItem* spinner = [[UIBarButtonItem alloc] initWithCustomView: activityIndicator];
    self.navigationItem.rightBarButtonItem = spinner;
    
    self.navigationItem.leftBarButtonItem = buttn;
    
    // Do any additional setup after loading the view from its nib.
}
-(void)backbuttonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated{

    [self fetchPriceDetailsInTimeInterval];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if (_arrayPriceDetails.count>3) {
//        return 6;
//    }
    return _arrayPriceDetails?[_arrayPriceDetails count]:0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell=(UITableViewCell*)[[[NSBundle mainBundle]loadNibNamed:@"PriceCell" owner:self options:nil]objectAtIndex:0];
    }
    [self customizeTableViewCell:cell atIndexPath:indexPath];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RTPIPriceDetailViewController *rtpiPriceDetailVC=[[RTPIPriceDetailViewController alloc]initWithNibName:@"RTPIPriceDetailViewController" bundle:nil];
    rtpiPriceDetailVC.dicData=[_arrayPriceDetails objectAtIndex:indexPath.row];
    rtpiPriceDetailVC.typeName=_type;
    rtpiPriceDetailVC.stringDate=[arrayDates objectAtIndex:indexPath.row];
    [timer invalidate];
    [self.navigationController pushViewController:rtpiPriceDetailVC animated:YES];
}

-(void)customizeTableViewCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicData=[_arrayPriceDetails objectAtIndex:indexPath.row];
    UIImageView *arrowView = (UIImageView *) [cell viewWithTag:1];
    UILabel *contractName  = (UILabel *) [cell viewWithTag:2];
    UILabel *contractValue  = (UILabel *) [cell viewWithTag:3];
    UILabel *contractChange  = (UILabel *) [cell viewWithTag:4];
    UILabel *contractPercent  = (UILabel *) [cell viewWithTag:5];
    UILabel *contractDate  = (UILabel *) [cell viewWithTag:6];
    UIImageView *disclosure = (UIImageView *)[cell viewWithTag:7];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            int monthInt=0;
            [formatter setDateFormat:@"M/d/yyyy h:m:s a"];
            [formatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"M/d/yyyy hh:mm:ss a" options:0 locale:[NSLocale currentLocale]]];
            NSLocale *twelveHourLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [formatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"M/d/yyyy hh:mm:ss a" options:0 locale:twelveHourLocale]];
            formatter.locale = twelveHourLocale;
            NSString *dateString=dicData[@"date"];
    NSArray *array = [dateString componentsSeparatedByString:@" "];
   NSString *str = array[1];
    NSArray *ar1 = [str componentsSeparatedByString:@":"];
   // [formatter dateFromString:dateString];
            [formatter setTimeZone:[NSTimeZone systemTimeZone]];
            NSDate *dt=[formatter dateFromString:dateString];
            formatter.dateFormat=@"h:mm";
            formatter.dateFormat=@"d";
            NSString *date = [formatter stringFromDate:dt];
            formatter.dateFormat=@"M";
            NSArray *arrayMonths=[dateString componentsSeparatedByString:@"/"];
            monthInt =[[arrayMonths objectAtIndex:0]integerValue];
            NSString *month=[mntharray objectAtIndex:monthInt-1];
                date=[arrayMonths objectAtIndex:1];
            if (![dicData[@"net"] isKindOfClass:[NSNull class]]) {
                float net=[dicData[@"net"]floatValue];
                NSString *netValueString = [NSString stringWithFormat:@"%.3f",net];
             //   netValueString= @"0.000";
                if([netValueString isEqualToString:@"0.000"]){
                     [arrowView setImage:[UIImage imageNamed:@"equalArrow"]];
                    arrowView.contentMode = UIViewContentModeScaleAspectFit;
                   // contractChange.textColor = [UIColor colorWithRed:255/255.0f green:192/255.0f blue:0/255.0f alpha:1];
                    contractChange.textColor = [UIColor blackColor];
                }
               else if (net>0) {
                    [arrowView setImage:[UIImage imageNamed:@"upArow.png"]];
    
                    contractChange.textColor = [UIColor colorWithRed:25.0f/255.0f green:150.0f/255.0f blue:0.0f alpha:1];
                               }
                else
                {
                    [arrowView setImage:[UIImage imageNamed:@"downarrow.png"]];
                    contractChange.textColor = [UIColor redColor];
                }
    
           // }
    
            if (![dicData[@"type"] isKindOfClass:[NSNull class]]) {
                //self.title = contracts.typeName;
                contractName.text   = dicData[@"f_month"];
                contractPercent.text=[NSString stringWithFormat:@"%@",dicData[@"PerChange"]];
                contractChange.text = [NSString stringWithFormat:@"%.3f", [dicData[@"net"]floatValue]];
    // contracts.change;
               //contractPercent.text = [NSString stringWithFormat:@"(%@)",contracts.percentage];
                [disclosure setImage:[UIImage imageNamed:@"disclosureIndicator.png"]];
              // NSString *strContractDate=[NSString stringWithFormat:@"%@ %@,%@",month,date,time];;
              contractDate.text = [NSString stringWithFormat:@"%@ %@,%@:%@",month,date,ar1[0],ar1[1]];
                [arrayDates addObject:contractDate.text];
                contractValue.text = [NSString stringWithFormat:@"%.2f",[dicData[@"last"]floatValue]];
            }
      //dat = [NSString stringWithFormat:@"%@ %@, %@",month,date,time];
}
}


-(void)fetchPriceDetailsInTimeInterval
{
    [activityIndicator startAnimating];
    user=[ALUser currentUser];
    
   // NSString *string = [_type stringByReplacingOccurrencesOfString:@" " withString:@"%20"];//[_type stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//               NSString *string = [_type stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//                NSString *link = "http://bc.savola.com/webservice/ListpriceTypeNew.aspx?type=";
    
               
//               link = link.replaceAll(" ", "%20");
    if ([_type isEqualToString:@"Palm Oil (BMD)"]) {
        _type=@"Palm  Oil (BMD)";
    }
    if ([_type isEqualToString:@"Palm Olein (FOB)"]) {
        _type=@"Palm  Olein (FOB)";
    }
    [user fetchPriceDetailsUsingPriceType:_type withCompletionBlock:^(BOOL success, id result, NSError *error) {
        
        if (success && ((NSArray *)result).count > 0){
            
            _arrayPriceDetails=[[NSArray alloc]initWithArray:result];
            [arrayDates removeAllObjects];
            [self.tableView reloadData];
        }
        NSMutableArray *arrayTemp = [[NSMutableArray alloc] initWithArray:_arrayPriceDetails];
        [arrayTemp sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            NSString *month1 = [[[[obj1 valueForKey:@"f_month"] componentsSeparatedByString:@"/"] firstObject] uppercaseString];
            NSString *month2 = [[[[obj2 valueForKey:@"f_month"] componentsSeparatedByString:@"/"] firstObject] uppercaseString];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MMM-yy"];
            NSDate *date1 = [dateFormatter dateFromString:month1];
            NSDate *date2 = [dateFormatter dateFromString:month2];
            return [date1 compare:date2];
        }];
        _arrayPriceDetails=[arrayTemp mutableCopy];
        [self.tableView reloadData];

        [activityIndicator stopAnimating];
        [self performSelector:@selector(fetchPriceDetailsInTimeInterval) withObject:nil afterDelay:2];
    }];
}


@end
