//
//  RTPINews.m
//  RTPI
//
//  Created by apple on 11/8/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import "RTPINews.h"

@implementation RTPINews

-(RTPINews *)initWithNewsDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if (self) {
        self.newsId          = [dictionary objectForKey:@"news_id"];
        self.newsTitle       = [dictionary objectForKey:@"title"];
        self.newsDescription = [dictionary objectForKey:@"description"];
        self.newsDate        = [dictionary objectForKey:@"date"];
    }
    return self;
}
@end
