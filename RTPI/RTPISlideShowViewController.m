//
//  RTPISlideShowViewController.m
//  RTPI
//
//  Created by SRISHTI INNOVATIVE on 14/03/14.
//  Copyright (c) 2014 Abhishek. All rights reserved.
//

#import "RTPISlideShowViewController.h"
#import "RTPILoginViewController.h"
#import "RTPIAppDelegate.h"
#import "ALUser.h"
#import "RTPIPriceDetails.h"
@interface RTPISlideShowViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property(nonatomic,retain)ALUser *user;
@property(nonatomic,assign)BOOL isGetResult;
@property(nonatomic,retain)RTPIPriceDetails *rtpiPriceDetails;
@property(nonatomic,retain)NSArray *arrayOfObjects;
@end

@implementation RTPISlideShowViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _user=[ALUser currentUser];
    //[self fetchPriceDetails];
    [_imageView setAlpha:0];
    [self.navigationController.navigationBar setHidden:YES];
    [UIView animateWithDuration:4 animations:^
    {
        [_imageView setAlpha:1];

    } completion:^(BOOL finished) {
       [UIView animateWithDuration:3 animations:^{
           [_imageView setAlpha:0];
       } completion:^(BOOL finished) {
           [_imageView setImage:[UIImage imageNamed:@"slideShowImg1_5.jpg"]];
       }];
                [UIView animateWithDuration:3 animations:^{
                    [_imageView setAlpha:1];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:3 animations:^{
                    } completion:^(BOOL finished) {
                        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLoggedIn"])
                        {
                            [self performSelector:@selector(pushToHomeView) withObject:nil afterDelay:1];
                        }
                       else
                       {
                       [self performSelector:@selector(pushtoLogInView) withObject:nil afterDelay:1];
                       }
                    }];
                }];
            }];

}

-(void)fetchPriceDetails
{
    _arrayOfObjects=[NSArray new];
    [_user fetchPriceDetailsWithIdentifier:1 withCompletionBlock:^(BOOL success, id result, NSError *error) {
        _rtpiPriceDetails=[[RTPIPriceDetails alloc]init] ;
        _rtpiPriceDetails=result;
        _isGetResult=YES;
        if (success) {
            _arrayOfObjects=result;
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLoggedIn"])
            {
                [self performSelector:@selector(pushToHomeView) withObject:nil afterDelay:1];
            }
            else
            {
                [self performSelector:@selector(pushtoLogInView) withObject:nil afterDelay:1];
            }

        }
    }];
}

-(void)pushToHomeView
{
        RTPIAppDelegate *delegate = [UIApplication sharedApplication].delegate;
        [delegate didLogin_priceDetails:_rtpiPriceDetails priceListDetails:_arrayOfObjects];
}

-(void)pushtoLogInView
{
    RTPILoginViewController *loginViewController = [[RTPILoginViewController alloc]initWithNibName:@"RTPILoginViewController" bundle:nil];
    loginViewController.rtpiPriceDetails=_rtpiPriceDetails;
    loginViewController.arrayOfObjects=_arrayOfObjects;
    loginViewController.deviceToken=_deviceToken;
    [self.navigationController pushViewController:loginViewController animated:NO];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
