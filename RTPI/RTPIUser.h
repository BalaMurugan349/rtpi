//
//  RTPIUser.h
//  RTPI
//
//  Created by apple on 11/7/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTPIUser : NSObject

@property (nonatomic,strong)NSString *userName;
@property (nonatomic,strong)NSString *userID;
@property (nonatomic,strong)NSString *password;
@property (nonatomic,strong)NSArray  *priceTypeArray;
@property (nonatomic,strong)NSArray  *priceTypeIdArray;
@property (nonatomic,strong)NSString *emailId;
@property int swipeCount;

+(RTPIUser *)currentUser;
-(void)saveUserDetails;

@end
