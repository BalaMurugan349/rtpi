//
//  LDServiceManager.h
//  LoginDemo
//
//  Created by Siju karunakaran on 10/3/13.
//  Copyright (c) 2013 Srishti Innovative. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ALServiceManager : NSObject
+(void)fetchDataFromService:(NSString *)serviceName withParameters:(id)parameters withCompletionBlock:(void(^)(BOOL ,id , NSError *))completion;
+(void)handleError:(NSError*)error;
@end
