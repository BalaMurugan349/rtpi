//
//  RTPIPriceDetails.m
//  RTPI
//
//  Created by SRISHTI INNOVATIVE on 07/06/14.
//  Copyright (c) 2014 Abhishek. All rights reserved.
//
#import "RTPIPriceDetails.h"

@implementation RTPIPriceDetails
-(id)initWithArray:(NSArray *)arrayPriceDetails
{
    self=[super init];
    if (self) {
        _arrayPalmOilBMD=[NSMutableArray new];
        _arrayPalmOilFOB=[NSMutableArray new];
        _arrayRawSugar=[NSMutableArray new];
        _arraySoybeanFOB=[NSMutableArray new];
        _arraySoybeanOilCME=[NSMutableArray new];
        _arrayWhiePremium=[NSMutableArray new];
        _arrayWhiteSugar=[NSMutableArray new];
        [arrayPriceDetails enumerateObjectsUsingBlock:^(NSDictionary *dicData, NSUInteger idx, BOOL *stop) {
            NSString *trimmedString = [dicData[@"type"] stringByReplacingOccurrencesOfString:@" " withString:@""];
            if ([trimmedString isEqualToString:@"WhiteSugar"]) {
                [_arrayWhiteSugar addObject:dicData];
            }
            else if ([trimmedString isEqualToString:@"WhitePremiums"])
            {
                [_arrayWhiePremium addObject:dicData];
            }
            else if ([trimmedString isEqualToString:@"SoybeanOil(FOB)"])
            {
                [_arraySoybeanFOB addObject:dicData];
            }
            else if ([trimmedString isEqualToString:@"SoybeanOil(CME)"])
            {
                [_arraySoybeanOilCME addObject:dicData];
            }
            else if ([trimmedString isEqualToString:@"PalmOlein(FOB)"])
            {
                [_arrayPalmOilFOB addObject:dicData];
            }
            else if ([trimmedString isEqualToString:@"RawSugar"])
            {
                [_arrayRawSugar addObject:dicData];
            }
            else if ([trimmedString isEqualToString:@"PalmOil(BMD)"])
            {
                [_arrayPalmOilBMD addObject:dicData];
            }
        }];
    }
    return self;
}
@end
