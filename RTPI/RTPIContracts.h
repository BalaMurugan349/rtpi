//
//  RTPIContracts.h
//  RTPI
//
//  Created by sics on 07/12/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTPIContracts : NSObject

@property (nonatomic,strong) NSString *typeId;
@property (nonatomic,strong) NSString *typeName;
@property (nonatomic,strong) NSString *contractMonth;
@property (nonatomic,strong) NSString *last;
@property (nonatomic,strong) NSString *change;
@property (nonatomic,strong) NSString *open;
@property (nonatomic,strong) NSString *low;
@property (nonatomic,strong) NSString *high;
@property (nonatomic,strong) NSString *percentage;
@property (nonatomic,strong) NSString *date;
@property (nonatomic,strong) NSString *volume;
@property (nonatomic,strong) NSString *contractId;

-(id)initWithDictionary:(NSDictionary *)dictionary;

@end
