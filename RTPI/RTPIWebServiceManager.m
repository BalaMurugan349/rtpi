//
//  RTPIWebServiceManager.m
//  RTPI
//
//  Created by apple on 11/7/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import "RTPIWebServiceManager.h"
#import "RTPIContracts.h"

@implementation RTPIWebServiceManager


-(id)initWithUrl:(NSString *)url andId:(int)identifier{
    self = [super init];
    if (self)
    {
        if(identifier==5)
        {
            NSString *urlToParse = [[NSString stringWithFormat:@"http://bc.savola.com/webservice/%@",url]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            urlIdntifier =identifier;
            [self parsingDidStartWithUrl:urlToParse];
        }
        else
        {
        NSString *urlToParse = [[NSString stringWithFormat:@"%@%@",FixedJson,url]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        urlIdntifier =identifier;
        [self parsingDidStartWithUrl:urlToParse];
    }
    }
    return self;
}
#pragma mark -StratParsing
-(void)parsingDidStartWithUrl:(NSString *)url
{
    NSURLRequest *uRLRequest1 = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:url]];
    NSURLConnection *urlConnection1 = [[NSURLConnection alloc]initWithRequest:uRLRequest1 delegate:self];
   [urlConnection1 start];

}
#pragma mark UrlConnectionDelgate
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
   urlData = [NSMutableData data];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [urlData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    //NSDictionary *urlDataDictionary = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
    id jsonResult = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
    switch (urlIdntifier) {
        case 3:
        {
            rtpiContractDataModelarray = [[NSMutableArray alloc]init];
            
            for (NSDictionary *dictionary in jsonResult) {
                RTPIContracts *contracts = [[RTPIContracts alloc]initWithDictionary:dictionary];
                [rtpiContractDataModelarray addObject:contracts];
            }
            [self.webServicedelegateForPrice didFinishParsingWithResultForPrice:rtpiContractDataModelarray];
        }
            break;
        default:
        {
            [self.webServiceDelegate didFinishParsingWithResult:jsonResult];
        }
            break;
    }
 }

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self.webServiceDelegate didFinishParsingWithResult:nil];
    if (![self doesAlertViewExist]) {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"RTPI couldn't connect to server.Please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        NSLog(@"%@",error.localizedDescription);
    }
}
-(BOOL) doesAlertViewExist {
    for (UIWindow* window in [UIApplication sharedApplication].windows) {
        NSArray* subviews = window.subviews;
        if ([subviews count] > 0) {
            
            BOOL alert = [[subviews objectAtIndex:0] isKindOfClass:[UIAlertView class]];
            BOOL action = [[subviews objectAtIndex:0] isKindOfClass:[UIActionSheet class]];
            
            if (alert || action)
                return YES;
        }
    }
    return NO;
}

@end
