//
//  RTPIQueryViewController.m
//  RTPI
//
//  Created by apple on 12/7/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import "RTPIQueryViewController.h"
#import "RTPIUser.h"
#import "RTPIWebServiceManager.h"
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"

@interface RTPIQueryViewController ()<RTPIWebserviceDelegate,UIPickerViewDelegate,UIActionSheetDelegate,UITextViewDelegate>
{
    NSArray *queryType ;
    NSString *subject;
    UIPickerView *myPickerView;
    MBProgressHUD *Hud;
}
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailID;
@end

@implementation RTPIQueryViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Query";
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    
}
- (void)viewDidLoad
{
    [self.selectQueryButton setTitleColor:[UIColor greenColor] forState:UIControlStateHighlighted];
    [super viewDidLoad];
    [_lblUserName setText:[NSString stringWithFormat:@"User Name :%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userName"]]];
    [_lblEmailID setText:[NSString stringWithFormat:@"Email ID  :%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"emailId"]]];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    // Do any additional setup after loading the view from its nib.
    Hud = [[MBProgressHUD alloc]init];
    [self.navigationController.view addSubview:Hud];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:18]};
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.title = @"Query";
    self.queryTxtView.layer.borderWidth = 1.0f;
    self.queryTxtView.layer.borderColor = [[UIColor darkGrayColor]CGColor];
    [self.queryTxtView setDelegate:self];
    
    button.layer.borderWidth = .5;
    button.layer.borderColor = [[UIColor darkGrayColor]CGColor];
    [button.layer setCornerRadius:5];
    self.queryTxtView.text = @" Enter your comments here.....";
    self.queryTxtView.textColor = [UIColor lightGrayColor];
    [_queryTxtView.layer setCornerRadius:5];
    queryType = @[@"Query",@"Suggestion",@"Complaint"];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navgtnBackGround.png"] forBarMetrics:UIBarMetricsDefault];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Add Inputaccessory view

-(void)addInputAccessoryViewforTextView {
    UIView *inputview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 35)];
    inputview.backgroundColor = [UIColor blackColor];
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancelButton setBackgroundColor:[UIColor clearColor]];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateHighlighted];
    [cancelButton addTarget:self action:@selector(dismissKeyboard) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(self.view.frame.size.width-100, 0, 100, 30)];
    [inputview addSubview:cancelButton];
    self.queryTxtView.inputAccessoryView = inputview;
}

#pragma mark - dissmiss Keyboard
-(void)dismissKeyboard
{
    [self.queryTxtView resignFirstResponder];
}
#pragma mark -PostButtonPressed
- (IBAction)postButtonPressed:(id)sender
{
    [self.queryTxtView resignFirstResponder];
//    if (_txtUserName.text.length>0&&[self NSStringIsValidEmail:_txtEmailID.text])
//    {
        if ([self validateTextView:self.queryTxtView] &&! [self.queryTxtView.text isEqualToString:@" Enter your comments here....."]  ) {
            if (![button.titleLabel.text isEqualToString:@"Select Type"]) {
                [Hud show:YES];
//sicsglobal.co.in/solution/rtpi/PostQuery.aspx?from_userid=2&query_type=Suggestion&query=Interface%20looks%20a%20bit%20akward
                NSString *url = [NSString stringWithFormat:@"PostQuery.aspx?from_userid=%@&query_type=%@&query=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userId"],subject,self.queryTxtView.text];
                RTPIWebServiceManager *webServiceManager = [[RTPIWebServiceManager alloc]initWithUrl:url andId:0];
                [webServiceManager setWebServiceDelegate:self];
            }
            else
            {
                [self showAlertWithMessage:@"Please choose a query type." andTitle:@"Error"];
            }
        }
//    }
    else{
        [self showAlertWithMessage:@"Please Fill all Fields." andTitle:@"Error"];
    }
    
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(BOOL)validateTextView:(id)textView{
    NSString *rawString = [self.queryTxtView text];
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
    if ([trimmed length] == 0)
    {
        return NO;
    }
    else
    {
        return YES;
    }
    
}

#pragma mark - WebServiceDelegate

-(void)didFinishParsingWithResult:(id)result{
    [Hud hide:YES];
    if ([[result objectForKey:@"Description"] isEqualToString:@"Feedback sent"])
    {
        [self showAlertWithMessage:@"Feedback sent" andTitle:@"Success"];
        self.queryTxtView.text = @" Enter your comments here.....";
        self.queryTxtView.textColor = [UIColor lightGrayColor];
    }
    else{
        if (![self doesAlertViewExist])
            [self showAlertWithMessage:@"Please try again."andTitle:@"Failure"];
    }
}
-(BOOL) doesAlertViewExist {
    for (UIWindow* window in [UIApplication sharedApplication].windows) {
        NSArray* subviews = window.subviews;
        if ([subviews count] > 0) {
            
            BOOL alert = [[subviews objectAtIndex:0] isKindOfClass:[UIAlertView class]];
            BOOL action = [[subviews objectAtIndex:0] isKindOfClass:[UIActionSheet class]];
            
            if (alert || action)
                return YES;
        }
    }
    return NO;
}
#pragma  amrk - show alert
-(void)showAlertWithMessage:(NSString *)msg andTitle:(NSString *)title{
    [[[UIAlertView alloc]initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
}

#pragma  mark - SelectQueryButtonPressed

- (IBAction)selectedQueryPressed:(id)sender{
    NSString *other1 = @"Query";
    NSString *other2 = @"Suggestion";
    NSString *other3 = @"Complaint";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Choose your query type." delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:other1,other2,other3, nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet setDelegate:self];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}
#pragma mark - UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.selectQueryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    switch (buttonIndex) {
        case 0:
        {
            button.titleLabel.textColor = [UIColor blackColor];
            [button setTitle:@"Query" forState:UIControlStateNormal];
            subject = @"Query";
        }
            break;
        case 1:
        {
            self.selectQueryButton.titleLabel.textColor = [UIColor blackColor];
            [button setTitle:@"Suggestion" forState:UIControlStateNormal];
            subject = @"Suggestion";
        }
            break;
        case 2:
        {
            button.titleLabel.textColor = [UIColor blackColor];
            [button setTitle:@"Complaint" forState:UIControlStateNormal];
            
            subject = @"Complaint";
        }
            break;
        default:
            break;
    }
    
}
#pragma mark - Textview delegate
-(void)textViewDidBeginEditing:(UITextView *)textView{
    self.queryTxtView.textColor = [UIColor blackColor];
    if ([_queryTxtView.text isEqualToString:@" Enter your comments here....."]) {
        [self.queryTxtView setText:@" "];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (![self validateTextView:_queryTxtView.text]) {
        [self.queryTxtView setText:@" Enter your comments here....."];
        self.queryTxtView.textColor = [UIColor lightGrayColor];
    }
    
}


@end
