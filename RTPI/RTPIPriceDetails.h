//
//  RTPIPriceDetails.h
//  RTPI
//
//  Created by SRISHTI INNOVATIVE on 07/06/14.
//  Copyright (c) 2014 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTPIPriceDetails : NSObject
-(id)initWithArray:(NSArray *)arrayPriceDetails;
@property(nonatomic,retain)NSMutableArray *arrayRawSugar;
@property(nonatomic,retain)NSMutableArray *arrayWhiteSugar;
@property(nonatomic,retain)NSMutableArray *arrayWhiePremium;
@property(nonatomic,retain)NSMutableArray *arrayPalmOilBMD;
@property(nonatomic,retain)NSMutableArray *arrayPalmOilFOB;
@property(nonatomic,retain)NSMutableArray *arraySoybeanOilCME;
@property(nonatomic,retain)NSMutableArray *arraySoybeanFOB;
@end
