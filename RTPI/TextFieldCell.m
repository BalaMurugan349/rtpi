//
//  TextFieldCell.m
//  RTPI
//
//  Created by apple on 11/5/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import "TextFieldCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation TextFieldCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithPlaceholder:(NSString *)string{
    self = [super init];
    if (self) {
        self.textField = [[UITextField alloc]initWithFrame:CGRectMake(30, 5, [UIScreen mainScreen].bounds.size.width-60, 40)];
        [self.textField setPlaceholder:string];
        if ([string isEqualToString:@"Password"]) {
            [self.textField setSecureTextEntry:YES];
        }
        [self.contentView addSubview:self.textField];
        [self addBorderForTextField:self.textField];
        [self addLeftPaddingForTextField:self.textField];
        [self hideKeyBoardForTextField:self.textField];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return self;
}
-(id)initWithUpdateButton{
    self=[super init];
    if (self)
    {
        self.button=[[UIButton alloc]initWithFrame:CGRectMake(100, 0, 120, 40)];
        [self.contentView addSubview:_button];
        [_button setTitle:@"Update" forState:UIControlStateNormal];
        [_button.layer setCornerRadius:5];
    }
    return self;
}
#pragma mark -Add TextField Borders

-(void)addBorderForTextField :(UITextField *)txtField{
    txtField.layer.borderWidth = 1.0f;
  //  self.textField.layer.cornerRadius = 8.0f;
    txtField.layer.borderColor = [[UIColor colorWithRed:200.0f/255.0f green:200.0f/255.0f blue:200.0f/255.0f alpha:1]CGColor];
    
}

#pragma mark - Add Left Padding

-(void)addLeftPaddingForTextField:(UITextField *)txtfield{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 35)];
    [txtfield setLeftView:view];
    [txtfield setLeftViewMode:UITextFieldViewModeAlways];
    
}

#pragma mark - HideKeyBoard 

-(void)hideKeyBoardForTextField : (UITextField *)txtField{
    [txtField addTarget:self action:@selector(hideKeyBoard) forControlEvents:UIControlEventEditingDidEndOnExit];
}

-(void)hideKeyBoard{
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
