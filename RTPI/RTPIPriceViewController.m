
//
//  RTPIPriceViewController.m
//  RTPI
//
//  Created by apple on 11/6/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import "RTPIPriceViewController.h"
#import "RTPIPriceDetailViewController.h"
#import "RTPIWebServiceManager.h"
#import "RTPIUser.h"
#import "MBProgressHUD.h"
#import "RTPIContracts.h"
#import "CustomPageView.h"
#import "RTPISEttingzViewController.h"
#import "RTPIAppDelegate.h"
#import "RTPILoginViewController.h"
#import "ALUser.h"
#import "RTPIPriceListCell.h"
#import "RTPISelectePriceDetailViewController.h"
@interface RTPIPriceViewController ()<UITableViewDataSource,UITableViewDelegate,NSURLConnectionDelegate,RTPIWebserviceDelegateForPricePage,CustomPageViewDelegate>{
    UITableViewCell *priceCell;
    NSMutableData *urlData;
    NSArray *priceType;
    MBProgressHUD *HUD;
    CustomPageView *pageView;
    NSString *strContractDate;
    int count;
    UIActivityIndicatorView *activityIndicator;
    UIRefreshControl *refreshControl;
//    NSMutableArray *arrayTemp;
    NSArray *OrginalArray;
}
@property(nonatomic,retain)ALUser *user;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBackground;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel *lblTableViewStatus;
- (IBAction)selectPage:(id)sender;

@end

@implementation RTPIPriceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //[self customiseNavigationBar];
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated{
}

-(void)viewWillAppear:(BOOL)animated
{
    
    RTPIAppDelegate *delegate = [UIApplication sharedApplication].delegate;
    delegate.isEnteredGraph = NO;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navgtnBackGround.png"] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - Customise Navigation Bar
-(void)customiseNavigationBar{
    [[UINavigationBar appearance]setBarTintColor:[UIColor colorWithRed:59.0f/255.0f green:158.0f/255.0f blue:40.0/255.0f alpha:1]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addRefreshControl];
    [self.navigationController setNavigationBarHidden:NO];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton *settingsButn = [UIButton buttonWithType:UIButtonTypeCustom];
    [settingsButn setFrame:CGRectMake(0, 0, 22, 19)];
    [settingsButn setImage:[UIImage imageNamed:@"profile.png"] forState:UIControlStateNormal];
    [settingsButn setImage:[UIImage imageNamed:@"profile.png"] forState:UIControlStateHighlighted];
    [settingsButn addTarget:self action:@selector(settingsPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *buttn = [[UIBarButtonItem alloc]initWithCustomView:settingsButn];
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:Nil action:nil];
    fixedSpace.width = 35.0f;
    
    activityIndicator = [[UIActivityIndicatorView alloc]
                         initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhite];
    // I've tried Gray, White, and WhiteLarge
    activityIndicator.hidden = NO;
    UIBarButtonItem* spinner = [[UIBarButtonItem alloc] initWithCustomView: activityIndicator];
    self.navigationItem.leftBarButtonItem = spinner;
    self.navigationItem.rightBarButtonItems = @[buttn,fixedSpace];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName :[UIFont boldSystemFontOfSize:18]};
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.navigationItem setTitle:@"Commodity"];
    [self fetchPriceDetails];
    priceType= [[NSMutableArray alloc]initWithObjects:@"Raw Sugar",@"White Sugar",@"White Premiums",@"Palm Oil (BMD)",@"Palm Olein (FOB)",@"Soybean Oil (CME)",@"Soybean Oil (FOB)", nil];
}

-(void)addRefreshControl{
//    arrayTemp = [[NSMutableArray alloc]init];
    _arrayOfObjects = [[NSMutableArray alloc]init];
    refreshControl = [[UIRefreshControl alloc]init];
    [_priceTable addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventValueChanged];
    
}

-(void)refreshAction{
    [refreshControl endRefreshing];
    [self fetchPriceDetails];
}

-(void)fetchPriceDetails
{
//    [arrayTemp removeAllObjects];
    
    _user=[ALUser currentUser];
    [activityIndicator startAnimating];
    [_user fetchPriceDetailsWithIdentifier:1 withCompletionBlock:^(BOOL success, id result, NSError *error) {
        [HUD hide:YES];
        if (success) {
            [activityIndicator stopAnimating];
            if ([(NSArray *)result count]>0) {
                NSArray *resultArray = (NSArray *)result;
                OrginalArray =resultArray;
                NSSet *setTypes = [[NSSet alloc] initWithArray:[resultArray valueForKey:@"type"]];
                [_arrayOfObjects removeAllObjects];
                [setTypes enumerateObjectsUsingBlock:^(NSString *_Nonnull obj, BOOL * _Nonnull stop) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type == %@", obj];
                    NSMutableArray *filtered = [[NSMutableArray alloc] initWithArray:[resultArray filteredArrayUsingPredicate:predicate]];
                    [filtered sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                        NSString *month1 = [[[[obj1 valueForKey:@"f_month"] componentsSeparatedByString:@"/"] firstObject] uppercaseString];
                        NSString *month2 = [[[[obj2 valueForKey:@"f_month"] componentsSeparatedByString:@"/"] firstObject] uppercaseString];
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"MMM-yy"];
                        NSDate *date1 = [dateFormatter dateFromString:month1];
                        NSDate *date2 = [dateFormatter dateFromString:month2];
                        return [date1 compare:date2];
                    }];
                    [_arrayOfObjects addObject:[filtered firstObject]];

                }];
                
//                [_arrayOfObjects addObjectsFromArray:arrayTemp];
                [_priceTable reloadData];
//                NSMutableArray *arrayType = [NSMutableArray new];
//                [resultArray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
//                    NSString *strType = [obj valueForKey:@"type"];
//                    if(arrayType.count == 0){
//                        [arrayType addObject:strType];
//                        [arrayTemp addObject:obj];
//                    }
//                    else if(![arrayType containsObject:strType]){
//                        [arrayType addObject:strType];
//                        [arrayTemp addObject:obj];
//                    }
//                    
//                    if(idx == resultArray.count - 1){
//                        [_arrayOfObjects removeAllObjects];
//                        [_arrayOfObjects addObjectsFromArray:arrayTemp];
//                        [_priceTable reloadData];
//                    }
//                    
//                }];
                
            }
        }
    }];
}

#pragma mark-RefreshPage
#pragma mark - TableViewDelegate And DataSources

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrayOfObjects.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"priceTypeCell"];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"priceTypeCell"];
        cell = [[[NSBundle mainBundle]loadNibNamed:@"CustomCellPriceList" owner:self options:nil]objectAtIndex:0];
    }
    [self customizeTableViewCell:cell atIndexPath:indexPath];
    return cell;
}

-(void)customizeTableViewCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexpath
{
    UIImageView *imgViewStatus = (UIImageView *) [cell viewWithTag:7];
    UILabel *lblPriceType  = (UILabel *) [cell viewWithTag:5];
    UILabel *lblF_Month  = (UILabel *) [cell viewWithTag:6];
    UILabel *lblLastPrice=(UILabel *)[cell viewWithTag:8];
    NSDictionary *dic=[_arrayOfObjects objectAtIndex:indexpath.row];
    //lblPriceType.text=[priceType objectAtIndex:indexpath.row];
    lblPriceType.text=[dic valueForKey:@"type"];
    
    
    //[NSString stringWithFormat:@"%.2f",[dicData[@"last"]floatValue]
    
    [lblF_Month setText:dic[@"f_month"]];
    [lblLastPrice setText:[NSString stringWithFormat:@"%.2f",[dic[@"last"]floatValue]]];
    float net=[dic[@"net"]floatValue];
    NSString *netValueString = [NSString stringWithFormat:@"%.3f",net];
    if([netValueString isEqualToString:@"0.000"]){
        [imgViewStatus setImage:[UIImage imageNamed:@"equalArrow"]];
        imgViewStatus.contentMode = UIViewContentModeScaleAspectFit;
    }
    
    else if (net>0) {
        [imgViewStatus setImage:[UIImage imageNamed:@"upArow.png"]];
    }
    else
    {
        [imgViewStatus setImage:[UIImage imageNamed:@"downarrow.png"]];
    }
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RTPISelectePriceDetailViewController *rtpiselectedPriceDetails=[[RTPISelectePriceDetailViewController alloc]initWithNibName:@"RTPISelectePriceDetailViewController" bundle:nil];
    NSArray *filteredArray = [OrginalArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(type == %@)", [_arrayOfObjects[indexPath.row] valueForKey:@"type"]]];
    NSMutableArray *arrayValues = [[NSMutableArray alloc] initWithArray:filteredArray];
    [arrayValues sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        NSString *month1 = [[[[obj1 valueForKey:@"f_month"] componentsSeparatedByString:@"/"] firstObject] uppercaseString];
        NSString *month2 = [[[[obj2 valueForKey:@"f_month"] componentsSeparatedByString:@"/"] firstObject] uppercaseString];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMM-yy"];
        NSDate *date1 = [dateFormatter dateFromString:month1];
        NSDate *date2 = [dateFormatter dateFromString:month2];
        return [date1 compare:date2];
    }];
    rtpiselectedPriceDetails.arrayPriceDetails = [arrayValues mutableCopy];
    rtpiselectedPriceDetails.type=[_arrayOfObjects[indexPath.row] valueForKey:@"type"];
    [self.navigationController pushViewController:rtpiselectedPriceDetails animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Settings button pressed
-(void)settingsPressed{
    RTPISEttingzViewController *settings = [[RTPISEttingzViewController alloc]initWithNibName:@"RTPISEttingzViewController" bundle:nil];
    [self.navigationController pushViewController:settings animated:YES];
}
@end
