//
//  RTPIContracts.m
//  RTPI
//  
//  Created by sics on 07/12/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import "RTPIContracts.h"

@implementation RTPIContracts

-(id)initWithDictionary:(NSDictionary *)dictionary{
    self  = [super init];
    if (self)
    {
        self.typeId = [dictionary objectForKey:@"Id"];
        self.typeName = [dictionary objectForKey:@"type"];
        self.contractMonth = [dictionary objectForKey:@"f_month"]
        ;
        self.last  = [dictionary objectForKey:@"last"];
        self.change = [dictionary objectForKey:@"net"];
        self.open = [dictionary objectForKey:@"Open"];
        self.low= [dictionary objectForKey:@"low"];
        self.high = [dictionary objectForKey:@"high"];
        self.date=[dictionary objectForKey:@"date"];
        self.contractId=[dictionary objectForKey:@"Contract_id"];
        self.percentage=[dictionary objectForKey:@"PerChange"];
    }
    return self;
}

@end
