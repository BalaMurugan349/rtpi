//
//  CustomPageView.m
//  RTPI
//
//  Created by apple on 12/18/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import "CustomPageView.h"

@implementation CustomPageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
       
    }
    return self;
}
-(id)initWithCustomPageViewWithFrame:(CGRect)frame andNumberOfButtons:(CGFloat)number{
    self = [super init];
    if (self) {
        
        [self setFrame:frame];
        [self setBackgroundColor:[UIColor colorWithRed:69.0f/255.0f green:158.0f/255.0f blue:12.0f/255.0f alpha:1]];
        
        
//        [self addButtonsToContainerView:number];
}
    return self;
 
}

-(void)addButtonsToContainerView:(CGFloat)numberOfbuttons
{
    NSMutableArray *btnsArray = [[NSMutableArray alloc]init];
    CGFloat width = self.frame.size.width;
    CGFloat startngPoint = width /(numberOfbuttons);
    for (int i = 1; i<=numberOfbuttons; i++)
    {
        
        UIButton *buttn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnsArray addObject:buttn];
        float x = (startngPoint* (numberOfbuttons- i))-5;

        if (i<numberOfbuttons/2&&i != numberOfbuttons/2) {
             [buttn setFrame:CGRectMake(x-width/2, 10, 20,20)];
            
        }
        
        else if(i>numberOfbuttons/2&&i != numberOfbuttons/2){
             [buttn setFrame:CGRectMake( width/2+x, 10, 20,20)];
        }
        else{
             [buttn setFrame:CGRectMake( width/2, 10, 20,20)];
        }

       
       buttn.tag = i;
        [buttn setBackgroundColor:[UIColor clearColor]];
        [buttn addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
       if (i == 1) {
           [buttn setBackgroundImage :[UIImage imageNamed:@"drkgry.png"] forState:UIControlStateNormal];
        }
       else{
            [buttn setBackgroundImage:[UIImage imageNamed:@"lgtgry.png"] forState:UIControlStateNormal];
//
       }
        [self addSubview:buttn];
    }
    _buttonsArray = [NSArray arrayWithArray:btnsArray];
    [_customPageViewDelegate customViewDidLoadWithButtons:_buttonsArray];
}
-(void)buttonPressed:(id)sender{
    UIButton *pressedButn = (UIButton *)sender;
    [self.customPageViewDelegate didSelectButtonAtIndex:pressedButn.tag OfButtonArray:self.buttonsArray ];
}

@end
