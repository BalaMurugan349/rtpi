//
//  LDUser.h
//  LoginDemo
//
//  Created by Siju karunakaran on 10/1/13.
//  Copyright (c) 2013 Srishti Innovative. All rights reserved.
//

#import <Foundation/Foundation.h>
            
@interface ALUser : NSObject

@property(nonatomic,strong)NSString *u_id;
@property(nonatomic,strong)NSString *u_name;
@property(nonatomic,retain)NSString *u_email;
@property(nonatomic,retain)NSString *u_admin;

+(ALUser *)currentUser;
+(void)logout;
+(void)loginWithEmail_id:(NSString *)emailId password:(NSString *)password withCompletionBlock:(void(^)(BOOL,ALUser *,NSError *))completionBlock;


-(void)fetchPriceDetailsWithIdentifier:(int)identifier withCompletionBlock:(void(^)(BOOL,id,NSError *))completionBlock;
-(void)fetchPriceDetailsUsingPriceType:(NSString *)priceType withCompletionBlock:(void(^)(BOOL,id,NSError *))completionBlock;
@end
