//
//  RTPIAppDelegate.m
//  RTPI
//
//  Created by apple on 11/5/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import "RTPIAppDelegate.h"
#import "RTPILoginViewController.h"
#import "RTPITabBarController.h"
#import "RTPIUser.h"
#import "RTPIPriceDetailViewController.h"
#import "RTPISlideShowViewController.h"
//#import <Crashlytics/Crashlytics.h>
@implementation RTPIAppDelegate
{
    NSString *strDeviceToken;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    RTPISlideShowViewController *slideShowViewController=[RTPISlideShowViewController new];
    slideShowViewController.deviceToken=strDeviceToken;
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:slideShowViewController];
    [self.window setRootViewController:navigationController];
    [self.window makeKeyAndVisible];
    CGRect frame =  self.window.bounds;
    navigationController.view.frame = CGRectMake(frame.origin.x, frame.origin.y+20, frame.size.width, frame.size.height);
//    RTPILoginViewController *loginViewController = [[RTPILoginViewController alloc]initWithNibName:@"RTPILoginViewController" bundle:nil];
//    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:loginViewController];
////    [self setStatusBarColor];
//    [self.window setRootViewController:navigationController];
//    [self.window makeKeyAndVisible];
//    CGRect frame =  self.window.bounds;
//    navigationController.view.frame = CGRectMake(frame.origin.x, frame.origin.y+20, frame.size.width, frame.size.height);
   // [Crashlytics startWithAPIKey:@"4bc6e0bfa37f920ac4d5403c2232f21fd1687f32"];
    
    // Let the device know we want to receive push notifications
	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    //urbanairship pushnotification
       return YES;
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
 //   strDeviceToken=[NSString stringWithFormat:@"%@",deviceToken];
    NSString* newToken = [[[NSString stringWithFormat:@"%@",deviceToken]
                           stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
	NSLog(@"My token is: %@", newToken);
    [[NSUserDefaults standardUserDefaults]setValue:newToken forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}
-(void)setStatusBarColor{
    
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 20)];
    [view setBackgroundColor:[UIColor blackColor]];
    [self.window addSubview:view];
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}
-(void)didLogin_priceDetails:(RTPIPriceDetails *)rtpiPriceDetails priceListDetails:(NSArray *)arrayOfObjetcs{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLoggedIn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    RTPITabBarController *tabBarController = [[RTPITabBarController alloc]initWithArray:arrayOfObjetcs];
    tabBarController.rtpiPriceDetails=rtpiPriceDetails;
    //tabBarController.arrayOfObjects=arrayOfObjetcs;
    [self.window setRootViewController:tabBarController];
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}
-(void)application:(UIApplication *)application didChangeStatusBarFrame:(CGRect)oldStatusBarFrame
{
}
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
   NSUInteger orientations =    UIInterfaceOrientationMaskPortrait;
    RTPITabBarController *tabbarController = (RTPITabBarController *)self.window.rootViewController;
    if ([self.window.rootViewController isKindOfClass:[RTPITabBarController class]]) {
        //UINavigationController *currentViewController = (UINavigationController*) tabbarController.selectedViewController;
        
            orientations = UIInterfaceOrientationMaskPortrait;
        
    }
    
    return orientations;

}
-(void)didLogout{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isLoggedIn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    RTPILoginViewController *loginViewController = [[RTPILoginViewController alloc]initWithNibName:@"RTPILoginViewController" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:loginViewController];
    //    [self setStatusBarColor];
    [self.window setRootViewController:navigationController];
}
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
@end
