//
//  RTPINews.h
//  RTPI
//
//  Created by apple on 11/8/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTPINews : NSObject

@property(nonatomic,strong) NSString *newsId;
@property(nonatomic,strong) NSString *newsTitle;
@property(nonatomic,strong) NSString *newsDescription;
@property(nonatomic,strong) NSString *newsDate;

-(RTPINews *)initWithNewsDictionary:(NSDictionary *)dictionary;

@end
