//
//  ButtonCell.h
//  RTPI
//
//  Created by apple on 11/5/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ButtonCell : UITableViewCell

@property(nonatomic,strong) UIButton *button1;
-(id)initWithButtonTitle:(NSString *)title;
@end
