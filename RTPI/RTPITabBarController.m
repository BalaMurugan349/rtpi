//
//  RTPITabBarController.m
//  RTPI
//
//  Created by apple on 11/6/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import "RTPITabBarController.h"
#import "RTPIPriceViewController.h"
#import "RTPINewsViewController.h"
#import "RTPIQueryViewController.h"

@interface RTPITabBarController ()

@end

@implementation RTPITabBarController

-(id)initWithArray:(NSArray *)array
{
    self = [super init];
    if (self) {
        //        CGRect tabBarframe = self.tabBar.frame;
        //        self.tabBar.frame = CGRectMake(tabBarframe.origin.x, tabBarframe.origin.y, tabBarframe.size.width, 60) ;
        //        [self setTabItems];
        UIImage *tabBarBackgd = [UIImage imageNamed:@"bottomBarBackGround.png"];
        tabBarBackgd = [tabBarBackgd imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.tabBar.backgroundImage = tabBarBackgd;
        self.tabBarItem.selectedImage = nil;
        self.tabBar.selectionIndicatorImage = nil;
        self.tabBar.selectedImageTintColor = [UIColor clearColor];
        self.arrayOfObjects = array;
        [self setTabItems];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tabBar setBarTintColor:[UIColor colorWithRed:56.0f/255.0f green:110.0f/255.0f blue:50.0f/255.0f alpha:1]];
    [self.tabBar setTranslucent:NO];
    //[self performSelector:@selector(setTabItems) withObject:nil afterDelay:1.0] ;
}

- (void)viewDidAppear:(BOOL)animated{
    
    
}
#pragma mark - SetTabItems
-(void)setTabItems
{
    RTPINewsViewController *newsViewController = [[RTPINewsViewController alloc]initWithNibName:@"RTPINewsViewController" bundle:nil];
    RTPIPriceViewController *priceViewController = [[RTPIPriceViewController alloc]initWithNibName:@"RTPIPriceViewController" bundle:nil];
    priceViewController.rtpiPriceDetails=_rtpiPriceDetails;
    priceViewController.arrayOfObjects=_arrayOfObjects;
    RTPIQueryViewController *queriesViewcontroller = [[RTPIQueryViewController alloc]initWithNibName:@"RTPIQueryViewController" bundle:nil];
    UINavigationController *newsDetailsController = [[UINavigationController alloc]initWithRootViewController:newsViewController];
    UINavigationController *priceDetailsController = [[UINavigationController alloc]initWithRootViewController:priceViewController];
    UINavigationController *queryDetailsViewcontroller = [[UINavigationController alloc]initWithRootViewController:queriesViewcontroller];
    [self setViewControllers:@[priceDetailsController,newsDetailsController,queryDetailsViewcontroller] animated:YES];
    [self setTabBarTitles];
}

-(void)setTabBarTitles{
    UIImage *priceIcon = [UIImage imageNamed:@"price_white.png"];
    UIImage *newsIcon = [UIImage imageNamed:@"news_white.png"];
    UIImage *queriesIcon = [UIImage imageNamed:@"Query_white.png"];
    UIImage *highlightedPriceIcon = [UIImage imageNamed:@"price_hover.png"];
    UIImage *highlightedNewsIcon = [UIImage imageNamed:@"news_hover.png"];
    UIImage *highlightdQueriesIcon = [UIImage imageNamed:@"Query_hover.png"];
    NSArray *titleArray1 = @[highlightedPriceIcon,highlightedNewsIcon, highlightdQueriesIcon];
    NSArray *titleArray = @[priceIcon,newsIcon,queriesIcon];
    for (int i = 0; i< titleArray.count; i++) {
        UINavigationController *nav = [self.viewControllers objectAtIndex:i];
        nav.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        nav.tabBarItem. title = nil;
        [nav.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, 90)];
        nav.tabBarItem.image = [[titleArray objectAtIndex:i]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [nav.tabBarItem setSelectedImage:[[titleArray1 objectAtIndex:i] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
