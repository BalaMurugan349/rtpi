//
//  RTPIPriceDetailForType.m
//  RTPI
//
//  Created by SRISHTI INNOVATIVE on 10/06/14.
//  Copyright (c) 2014 Abhishek. All rights reserved.
//

#import "RTPIPriceDetailForType.h"

@implementation RTPIPriceDetailForType
-(id)initWithDictionary:(NSDictionary *)dicData
{
    self=[super init];
    if (self) {
            _Id=dicData[@"Id"];
            _Contract_id=dicData[@"Contract_id"];
            _type=dicData[@"type"];
            _f_month=dicData[@"f_month"];
            _last=dicData[@"last"];
            _net=dicData[@"net"];
            _PerChange=dicData[@"PerChange"];
            _Open=dicData[@"Open"];
            _high=dicData[@"high"];
            _low=dicData[@"low"];
            _Old_Settlement=dicData[@"Old_Settlement"];
            _date=dicData[@"date"];
            }
    return self;
}
-(id)initWithArray:(NSArray *)arrayData
{
    self=[super init];
    if (self) {
        [arrayData enumerateObjectsUsingBlock:^(NSDictionary *dicData, NSUInteger idx, BOOL *stop) {
            
        }];
    }
    return self;
}
@end
