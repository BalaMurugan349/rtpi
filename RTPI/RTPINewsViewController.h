//
//  RTPINewsViewController.h
//  RTPI
//
//  Created by apple on 11/6/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTPINewsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *newsTableView;

@end
