//
//  RTPIPriceViewController.h
//  RTPI
//
//  Created by apple on 11/6/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTPIPriceDetails.h"
@interface RTPIPriceViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *priceTable;
@property (strong, nonatomic) IBOutlet UIButton *firstButton;
@property (strong, nonatomic) IBOutlet UIButton *secondButton;
@property (strong, nonatomic) IBOutlet UIButton *thirdButton;
@property (strong, nonatomic) IBOutlet UIButton *fourthButton;
@property (strong, nonatomic) IBOutlet UIButton *fifthButton;
@property(nonatomic,retain)RTPIPriceDetails *rtpiPriceDetails;
@property(nonatomic,retain) NSMutableArray *arrayOfObjects;
@end
