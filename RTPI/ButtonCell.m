//
//  ButtonCell.m
//  RTPI
//
//  Created by apple on 11/5/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import "ButtonCell.h"

@implementation ButtonCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(id)initWithButtonTitle:(NSString *)title{
    self = [super init];
    if (self) {
        self.button1 = [UIButton buttonWithType:UIButtonTypeSystem];
        [self.button1 setFrame:CGRectMake(60, 5, [UIScreen mainScreen].bounds.size.width-120, 40)];
        self.button1.titleLabel.textColor = [UIColor blackColor];
        
        [self.button1 setTitle:title forState:UIControlStateNormal];
        [self.button1 setTitle:title forState:UIControlStateHighlighted];
        
        [self.contentView addSubview:self.button1];
        
    }
    return self;
}


@end
