//
//  LDUser.m
//  LoginDemo
//
//  Created by Siju karunakaran on 10/1/13.
//  Copyright (c) 2013 Srishti Innovative. All rights reserved.
//

#import "ALUser.h"
#import "NSObject+PropertyDictionaryAddition.h"
#import "NSMutableData+PostDataAdditions.h"
#import "ALServiceManager.h"
#import "RTPIPriceDetails.h"
#import "RTPIPriceDetailForType.h"
static ALUser *_user = nil;
@implementation ALUser

-(id)initWithUserDetails:(NSDictionary *)userDetails{
    
    self = [super init];
    if (self) {
        self.u_id = userDetails[@"u_id"];
        self.u_name = userDetails[@"u_name"];
        self.u_email=userDetails[@"u_email"];
        self.u_admin=userDetails[@"u_admin"];
        [self setValuesForKeysWithDictionary:userDetails];
    }
    return self;
}
-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}
+(ALUser*)currentUser{
    
    @synchronized([ALUser class]) {
        
        return _user?_user:[[self alloc] init];
        
    }
    return nil;
}
+(void)logout{
    _user = nil;
}
+(id)alloc {
    @synchronized([ALUser class]) {
        
        NSAssert(_user == nil, @"Attempted to allocate a second instance of a singleton.");
        _user = [super alloc];
        return _user;
    
    }
    return nil;
}
+(ALUser*)userWithDetails:(NSDictionary *) userDetails{
    
    @synchronized([ALUser class])
    {
    
        return _user?_user:[[self alloc] initWithUserDetails:userDetails];
        
    }
    return nil;
}

+(void)loginWithEmail_id:(NSString *)emailId password:(NSString *)password withCompletionBlock:(void (^)(BOOL, ALUser *, NSError *))completionBlock
{
    NSMutableData *body=[NSMutableData postData];
    [body addValue:emailId forKey:@"email"];
    [body addValue:password forKey:@"password"];
    [ALServiceManager fetchDataFromService:@"login.php?" withParameters:body withCompletionBlock:^(BOOL success, id result, NSError *error) {
        if (success) {
            if ([result[@"result"]isEqualToString:@"success"]) {
                completionBlock(YES,[self userWithDetails:result[@"details"]],nil);
            }
            else
            {
                completionBlock(NO,nil,[NSError errorWithDomain:@"" code:1 userInfo:@{NSLocalizedDescriptionKey: result[@"result"]}]);
            }
        }
        else
        {
            NSLog(@"error: %@",error.localizedDescription);
            completionBlock(NO,nil,error);
        }
    }];
}

-(NSString *)makeFileName
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyMMddHHmmssSSS"];
    
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    int randomValue = arc4random() % 1000;
    
    NSString *fileName = [NSString stringWithFormat:@"%@%d.jpg",dateString,randomValue];
    
    return fileName;
}

-(void)fetchPriceDetailsWithIdentifier:(int)identifier withCompletionBlock:(void (^)(BOOL, id, NSError *))completionBlock
{
    [ALServiceManager fetchDataFromService:@"Currentprices.aspx?" withParameters:nil withCompletionBlock:^(BOOL success, id result, NSError *error) {
        if (success) {
           NSArray *arrayTemp = result;
           NSMutableArray *arrayTotal = [[NSMutableArray alloc]init];
//           NSMutableArray *arrayDiff = [NSMutableArray new];
//            
//           NSString *strOld;
//            int idx = 0;
//            NSMutableArray *arrayType = [NSMutableArray new];
//             for (NSDictionary*dict in arrayTemp) {
//                   NSString *strType = [dict valueForKey:@"type"];
//                 if(arrayType.count == 0){
//                     [arrayType addObject:strType];
//                     [arrayTotal addObject:dict];
//                 }
//                 else if(![arrayType containsObject:strType]){
//                     [arrayType addObject:strType];
//                     [arrayTotal addObject:dict];
//                 }
//                }
//            
           //           for (NSDictionary*dict in arrayTemp) {
//               NSString *strType = [dict valueForKey:@"type"];
//                   if ([strType isEqualToString:strOld]) {
//                       [arrayDiff addObject:dict];
//                       if (idx==arrayTemp.count-1) {
//                           if (arrayDiff.count>1) {
//                               NSMutableArray *array = [arrayDiff copy];
//                               [arrayTotal addObject:array];
//                           }
//                       }
//                   }else{
//                       if (arrayDiff.count>=1) {
//                               NSMutableArray *array = [arrayDiff copy];
//                               [arrayTotal addObject:array];
//                       }
//                       [arrayDiff removeAllObjects];
//                       [arrayDiff addObject:dict];
//                   }
//               strOld = strType;
//               idx++;
//           }
           
//            RTPIPriceDetails *rtpiPriceDetails=[[RTPIPriceDetails alloc]initWithArray:result];
//            NSArray *arrayOfObjects=@[rtpiPriceDetails.arrayRawSugar,rtpiPriceDetails.arrayWhiteSugar,rtpiPriceDetails.arrayWhiePremium,rtpiPriceDetails.arrayPalmOilBMD,rtpiPriceDetails.arrayPalmOilFOB,rtpiPriceDetails.arraySoybeanOilCME,rtpiPriceDetails.arraySoybeanFOB];
            completionBlock(YES,arrayTemp,nil);
        }
        else
        completionBlock(NO,nil,nil);
    }];
}
-(void)fetchPriceDetailsUsingPriceType:(NSString *)priceType withCompletionBlock:(void (^)(BOOL, id, NSError *))completionBlock
{
    NSMutableData *data=[NSMutableData postData];
    [data addValue:priceType forKey:@"type"];
    NSLog(@"Url http://bc.savola.com/webservice/ListpriceTypeNew.aspx?type=%@",priceType);
    [ALServiceManager fetchDataFromService:@"ListpriceTypeNew.aspx?" withParameters:data withCompletionBlock:^(BOOL success, id result, NSError *error) {
        if (success) {
            NSMutableArray *arrayOfObjects=[NSMutableArray new];
            [result enumerateObjectsUsingBlock:^(NSDictionary *dicData, NSUInteger idx, BOOL *stop) {
                RTPIPriceDetailForType *rtpiPriceDetails=[[RTPIPriceDetailForType alloc]initWithDictionary:dicData];
                [arrayOfObjects addObject:rtpiPriceDetails];
            }];
            completionBlock(YES,result,nil);
        }
        else
        {
            completionBlock(NO,nil,nil);
        }
        
    }];
}
@end
