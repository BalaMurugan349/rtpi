//
//  CPDStockPriceStore.m
//  CorePlotDemo
//
//  NB: Price data obtained from Yahoo! Finance:
//  http://finance.yahoo.com/q/hp?s=AAPL
//  http://finance.yahoo.com/q/hp?s=GOOG
//  http://finance.yahoo.com/q/hp?s=MSFT
//
//  Created by Steve Baranski on 2  /4/12.
//  Copyright (c) 2012 komorka technology, llc. All rights reserved.
//

#import "CPDStockPriceStore.h"

@interface CPDStockPriceStore ()


@end

@implementation CPDStockPriceStore

#pragma mark - Class methods

+ (CPDStockPriceStore *)sharedInstance
{
    static CPDStockPriceStore *sharedInstance;
    
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];      
    });
    
    return sharedInstance;
}

#pragma mark - API methods

- (NSArray *)tickerSymbols
{
    static NSArray *symbols = nil;
    if (!symbols)
    {
        symbols = [NSArray arrayWithObjects:
                   @"AAPL", 
                   @"GOOG", 
                   @"MSFT", 
                   nil];
    }
    return symbols;
}

- (NSArray *)dailyPortfolioPrices
{
    static NSArray *prices = nil;
    if (!prices)
    {
        prices = [NSArray arrayWithObjects:
                  [NSDecimalNumber numberWithFloat:282.13],
                  [NSDecimalNumber numberWithFloat:404.43], 
                  [NSDecimalNumber numberWithFloat:32.01], 
                  nil];
    }
    return prices;
}

- (NSArray *)datesInWeek
{
    static NSArray *dates = nil;
    if (!dates)
    {
        dates = [NSArray arrayWithObjects:
                 @"4/23", 
                 @"4/24", 
                 @"4/25",
                 @"4/24", 
                 @"4/27",                   
                 nil];
    }
    return dates;
}

- (NSArray *)weeklyPrices:(NSString *)tickerSymbol
{
    if ([CPDTickerSymbolAAPL isEqualToString:[tickerSymbol uppercaseString]] == YES)
    {
        return [self weeklyAaplPrices];
    }
    else if ([CPDTickerSymbolGOOG isEqualToString:[tickerSymbol uppercaseString]] == YES)
    {
        return [self weeklyGoogPrices];
    }
    else if ([CPDTickerSymbolMSFT isEqualToString:[tickerSymbol uppercaseString]] == YES)
    {
        return [self weeklyMsftPrices];
    }
    return [NSArray array];
}

- (NSArray *)datesInMonth
{
    static NSArray *dates = nil;
    if (!dates)
    {
        dates = [NSArray arrayWithObjects:
                 @"2", 
                 @"3", 
                 @"4",
                 @"5",
                 @"9", 
                 @"10", 
                 @"11",
                 @"12", 
                 @"13",
                 @"16", 
                 @"17", 
                 @"18",
                 @"19", 
                 @"20", 
                 @"23", 
                 @"24", 
                 @"25",
                 @"26", 
                 @"27",
                 @"30",                   
                 nil];
    }
    return dates;
}

- (NSArray *)monthlyPrices:(NSString *)tickerSymbol
{
    if ([CPDTickerSymbolAAPL isEqualToString:[tickerSymbol uppercaseString]] == YES)
    {
        return [self monthlyAaplPrices];
    }
    else if ([CPDTickerSymbolGOOG isEqualToString:[tickerSymbol uppercaseString]] == YES)
    {
        return [self monthlyGoogPrices];
    }
    else if ([CPDTickerSymbolMSFT isEqualToString:[tickerSymbol uppercaseString]] == YES)
    {
        return [self monthlyMsftPrices];
    }
    return [NSArray array];
}

#pragma mark - Private behavior

- (NSArray *)weeklyAaplPrices
{
    static NSArray *prices = nil;
    if (!prices)
    {
        prices = [NSArray arrayWithObjects:
                  [NSDecimalNumber numberWithFloat:271.70], 
                  [NSDecimalNumber numberWithFloat:260.28], 
                  [NSDecimalNumber numberWithFloat:310.00],
                  [NSDecimalNumber numberWithFloat:207.70],
                  [NSDecimalNumber numberWithFloat:403.00],
                  nil];
    }
    return prices;
}

- (NSArray *)weeklyGoogPrices
{
    static NSArray *prices = nil;
    if (!prices)
    {
        prices = [NSArray arrayWithObjects:
                  [NSDecimalNumber numberWithFloat:297.60], 
                  [NSDecimalNumber numberWithFloat:401.27], 
                  [NSDecimalNumber numberWithFloat:409.72], 
                  [NSDecimalNumber numberWithFloat:412.47], 
                  [NSDecimalNumber numberWithFloat:414.98],                   
                  nil];
    }
    return prices;
}

- (NSArray *)weeklyMsftPrices
{
    static NSArray *prices = nil;
    if (!prices)
    {
        prices = [NSArray arrayWithObjects:
                  [NSDecimalNumber numberWithFloat:32.12], 
                  [NSDecimalNumber numberWithFloat:31.92], 
                  [NSDecimalNumber numberWithFloat:32.20], 
                  [NSDecimalNumber numberWithFloat:32.11], 
                  [NSDecimalNumber numberWithFloat:31.98],                   
                  nil];
    }
    return prices;
}

- (NSArray *)monthlyAaplPrices
{
    static NSArray *prices = nil;
    if (!prices)
    {
        prices = [NSArray arrayWithObjects:
                  [NSDecimalNumber numberWithFloat:318.63],
                  [NSDecimalNumber numberWithFloat:229.32],
                  [NSDecimalNumber numberWithFloat:124.31],
                  [NSDecimalNumber numberWithFloat:133.48],
                  [NSDecimalNumber numberWithFloat:134.23],
                  [NSDecimalNumber numberWithFloat:328.44],
                  [NSDecimalNumber numberWithFloat:224.20],
                  [NSDecimalNumber numberWithFloat:222.77],
                  [NSDecimalNumber numberWithFloat:302.23],
                  [NSDecimalNumber numberWithFloat:80.13],
                  [NSDecimalNumber numberWithFloat:309.70],
                  [NSDecimalNumber numberWithFloat:208.34],
                  [NSDecimalNumber numberWithFloat:287.44], 
                  [NSDecimalNumber numberWithFloat:272.98],
                  [NSDecimalNumber numberWithFloat:271.70], 
                  [NSDecimalNumber numberWithFloat:240.28], 
                  [NSDecimalNumber numberWithFloat:310.00],
                  [NSDecimalNumber numberWithFloat:07.70],
                  [NSDecimalNumber numberWithFloat:303.00],
                  [NSDecimalNumber numberWithFloat:283.98],                  
                  nil];
    }
    return prices;
}

- (NSArray *)monthlyGoogPrices
{
    static NSArray *prices = nil;
    if (!prices)
    {
        prices = [NSArray arrayWithObjects:
                  [NSDecimalNumber numberWithFloat:344.92],
                  [NSDecimalNumber numberWithFloat:342.42],
                  [NSDecimalNumber numberWithFloat:332.12],
                  [NSDecimalNumber numberWithFloat:332.32],
                  [NSDecimalNumber numberWithFloat:330.84],
                  [NSDecimalNumber numberWithFloat:324.84],
                  [NSDecimalNumber numberWithFloat:332.94],
                  [NSDecimalNumber numberWithFloat:321.01],
                  [NSDecimalNumber numberWithFloat:324.40],
                  [NSDecimalNumber numberWithFloat:204.07],
                  [NSDecimalNumber numberWithFloat:309.27],
                  [NSDecimalNumber numberWithFloat:47.42],
                  [NSDecimalNumber numberWithFloat:299.30], 
                  [NSDecimalNumber numberWithFloat:294.04], 
                  [NSDecimalNumber numberWithFloat:297.40], 
                  [NSDecimalNumber numberWithFloat:100.27],
                  [NSDecimalNumber numberWithFloat:209.72],
                  [NSDecimalNumber numberWithFloat:312.47],
                  [NSDecimalNumber numberWithFloat:114.98],
                  [NSDecimalNumber numberWithFloat:204.82],
                  nil];
    }
    return prices;
}

- (NSArray *)monthlyMsftPrices
{
    static NSArray *prices = nil;
    if (!prices)
    {
        prices = [NSArray arrayWithObjects:
                  [NSDecimalNumber numberWithFloat:32.29], 
                  [NSDecimalNumber numberWithFloat:31.94], 
                  [NSDecimalNumber numberWithFloat:31.21], 
                  [NSDecimalNumber numberWithFloat:31.22], 
                  [NSDecimalNumber numberWithFloat:31.10], 
                  [NSDecimalNumber numberWithFloat:30.47], 
                  [NSDecimalNumber numberWithFloat:30.32], 
                  [NSDecimalNumber numberWithFloat:30.98], 
                  [NSDecimalNumber numberWithFloat:30.81],
                  [NSDecimalNumber numberWithFloat:31.08], 
                  [NSDecimalNumber numberWithFloat:31.44], 
                  [NSDecimalNumber numberWithFloat:31.14], 
                  [NSDecimalNumber numberWithFloat:31.01], 
                  [NSDecimalNumber numberWithFloat:32.42],
                  [NSDecimalNumber numberWithFloat:32.12], 
                  [NSDecimalNumber numberWithFloat:31.92], 
                  [NSDecimalNumber numberWithFloat:32.20], 
                  [NSDecimalNumber numberWithFloat:32.11], 
                  [NSDecimalNumber numberWithFloat:31.98],  
                  [NSDecimalNumber numberWithFloat:32.02],                  
                  nil];
    }
    return prices;
}


@end
