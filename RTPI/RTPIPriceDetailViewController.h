//
//  RTPIPriceDetailViewController.h
//  RTPI
//
//  Created by apple on 11/6/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTPIContracts.h"

@interface RTPIPriceDetailViewController : UIViewController


@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UILabel *contractName;
@property (strong, nonatomic) IBOutlet UILabel *contrctValue;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *changeLabel;
@property (strong, nonatomic) IBOutlet UILabel *changePercentLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;

@property (strong, nonatomic) NSString *typeName;
@property (strong, nonatomic) NSString *typeId;
@property (strong, nonatomic) NSString *contractDate;
@property (strong, nonatomic) NSString *percentReplacingSpclCharactrs;
@property BOOL isPoppedFromGraphPage;
@property (nonatomic,strong)RTPIContracts *selectedContracts;
@property (strong, nonatomic) IBOutlet UITableView *detailsTableView;
@property(nonatomic,retain)NSDictionary *dicData;
@property(nonatomic,retain)NSString *stringDate;
@end
