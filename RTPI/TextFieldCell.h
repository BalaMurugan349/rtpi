//
//  TextFieldCell.h
//  RTPI
//
//  Created by apple on 11/5/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldCell : UITableViewCell

@property (nonatomic,strong) UITextField *textField;
@property(nonatomic,strong)UIButton *button;
-(id)initWithPlaceholder: (NSString *)string;
-(id)initWithUpdateButton;
@end
