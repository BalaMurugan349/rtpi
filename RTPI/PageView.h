//
//  PageView.h
//  RTPI
//
//  Created by apple on 12/18/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PageView : NSObject
@property (nonatomic,strong) UIView *containerView;

-(id)initWithCustomPageViewWithFrame :(CGRect)frame andNumberOfButtons :(int)number;
@end
