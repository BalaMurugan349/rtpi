//
//  LDServiceManager.m
//  LoginDemo
//
//  Created by Siju karunakaran on 10/3/13.
//  Copyright (c) 2013 Srishti Innovative. All rights reserved.
//

#import "ALServiceManager.h"
@interface ALServiceManager()<NSURLConnectionDataDelegate>
@end
@implementation ALServiceManager{
    
    NSMutableData *_urlData;
    NSString *_mainURl;
    NSString *urlSavola;
    void (^finishLoading)(BOOL ,id , NSError *);
    
}
-(id)init{
    self = [super init];
    if (self) {
      
    urlSavola=  @"http://bc.savola.com/webservice/";
        
        // urlSavola=  @"http://sicsglobal.co.in/solution/rtpi/";
    }
    return self;
}
+(void)fetchDataFromService:(NSString *)serviceName withParameters:(id)parameters withCompletionBlock:(void(^)(BOOL ,id , NSError *))completion{
    
    ALServiceManager *conectionRequest = [self new];
    
    [conectionRequest httpRequestForService:serviceName withParameters:parameters inCompletionBlock:^(BOOL success, id result, NSError *error) {
        completion(success,result,error);
    }];
    
}

+(void)handleError:(NSError*)error{
    
    [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
}
-(void)httpRequestForService:(NSString *)service withParameters:(NSData *)parameters inCompletionBlock:(void(^)(BOOL , id ,NSError *)) completion{
    
    finishLoading = completion;
    NSURL *url;
   // if ([service isEqualToString:@"ListpriceTypeNew.aspx?"]) {
    ////////
    /*
    if ([service isEqualToString:@"Currentprices.aspx?"]) {
        urlSavola=_mainURl;
    }
    */
    ////////////
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",urlSavola,service]];
        NSLog(@"url==== %@",url);
  //  }
   // else
//    {
//        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",_mainURl,service]];
//        NSLog(@"url==== %@",url);
//    }
    NSURLRequest *request = [self prepareRequestWithURL:url andParameters:parameters];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
    
}
 
-(NSURLRequest *)prepareRequestWithURL:(NSURL *)url andParameters:(NSData *) parameters{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:url];
    if (parameters) {
    [request setHTTPMethod:@"POST"];
    NSString *charset = (NSString *)CFStringConvertEncodingToIANACharSetName(CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; charset=%@; boundary=%@",charset, @"0xKhTmLbOuNdArY"];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setHTTPBody:parameters];
    }
return request;
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    _urlData = [NSMutableData data];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [_urlData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSError *error;
    id json = [NSJSONSerialization JSONObjectWithData:_urlData options:kNilOptions error:&error];
    finishLoading(json?YES:NO,json,error);
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    finishLoading(NO,nil,error);
    
}
@end
