//
//  RTPISEttingzViewController.m
//  RTPI
//
//  Created by apple on 1/18/14.
//  Copyright (c) 2014 Abhishek. All rights reserved.
//

#import "RTPISEttingzViewController.h"
#import "TextFieldCell.h"
#import "ButtonCell.h"
#import <QuartzCore/QuartzCore.h>
#import "RTPIUser.h"
#import "RTPIWebServiceManager.h"
#import "RTPIAppDelegate.h"
#import "MBProgressHUD.h"


@interface RTPISEttingzViewController ()<UITableViewDelegate,UITableViewDataSource,RTPIWebserviceDelegate>
{
    NSArray *firstSectionArray,*secondSectionArray;
    TextFieldCell *userNAmeField, *currentPasswordField, *newPAsswordField,*logoutField;
    ButtonCell *logOutButtonCell;
    MBProgressHUD *Hud;
}

@end

@implementation RTPISEttingzViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.settingsTable.tableHeaderView = [self setTableHeaderView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    Hud = [[MBProgressHUD alloc]init];
    [self.navigationController.view addSubview:Hud];
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setFrame:CGRectMake(0, 0, 60, 25)];
    [rightButton setImage:[UIImage imageNamed:@"LogOut.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightBarButtonItemPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarbutton = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = rightBarbutton;
  
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 57, 25)];
    
    [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(backbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *buttn = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = buttn;
    [self configureTableCells];
}
-(void)backbuttonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(UIView *)setTableHeaderView{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 10,  [UIScreen mainScreen].bounds.size.width-20, 30)];
    label.textColor = [UIColor darkGrayColor];
    [label setText:[RTPIUser currentUser].emailId];
    [label setFont:[UIFont boldSystemFontOfSize:18]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [headerView addSubview:label];
 
       return headerView;
}
-(void)configureTableCells{
    currentPasswordField = [[TextFieldCell alloc]initWithPlaceholder:@"Current password"];
    newPAsswordField = [[TextFieldCell alloc]initWithPlaceholder:@"New pasword"];
    [userNAmeField.layer setCornerRadius:5];
    [newPAsswordField.layer setCornerRadius:5];
    logoutField = [[TextFieldCell alloc]initWithUpdateButton];
        [logoutField.button setBackgroundImage:[UIImage imageNamed:@"logInBtnBackGround.png"] forState:UIControlStateNormal];
    [logoutField.button.layer setCornerRadius:5];
    [logoutField.button addTarget:self action:@selector(changeCredentials) forControlEvents:UIControlEventTouchUpInside];
   userNAmeField.backgroundColor = [UIColor clearColor];
   userNAmeField.textField.backgroundColor = [UIColor whiteColor];
   currentPasswordField.backgroundColor = [UIColor clearColor];
   currentPasswordField.textField.backgroundColor = [UIColor whiteColor];
   newPAsswordField.backgroundColor = [UIColor clearColor];
   newPAsswordField.textField.backgroundColor = [UIColor whiteColor];
    logoutField.backgroundColor = [UIColor clearColor];
    [currentPasswordField.layer setCornerRadius:5];
    [newPAsswordField.layer setCornerRadius:5];
    logoutField.textField.textColor = [UIColor whiteColor];
    logoutField.textField.layer.borderColor = [[UIColor clearColor]CGColor];
    firstSectionArray = @[currentPasswordField,newPAsswordField];
    secondSectionArray = @[logoutField];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - tableview delegate and data sources
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger rowCount = 0;
    switch (section) {
        case 0:
        {
            rowCount = firstSectionArray.count;
        }
            break;
            
            case 1:
        {
            rowCount = secondSectionArray.count;
        }
            break;
            
        default:
            break;
    }
    return rowCount;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell;
    switch (indexPath.section) {
        case 0:
            cell = [firstSectionArray objectAtIndex:indexPath.row];
            break;
            
            case 1:
            cell = [secondSectionArray objectAtIndex:indexPath.row];
            
        default:
            break;
    }
    
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *sectionHEader;
    if (section == 0) {
    }
    return sectionHEader;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
//    if (indexPath.section == 1) {
//        if ([self ValidateTextFields]) {
//            [self changeCredentials];
//        }
//    }
}
-(BOOL)ValidateTextFields{
    
    if (currentPasswordField.textField.text.length >0 && newPAsswordField.textField.text.length >0) {
        return YES;
    }
    else{
        [self showAlertWithTitle:@"Error" andMessage:@"Please fill all fields"];
        return NO;
    }
    
}
-(void)changeCredentials{
    if ([self ValidateTextFields])
    {
        NSString *url = [NSString stringWithFormat:@"Updatepassword.aspx?email=%@&oldpassword=%@&newpassword=%@",[RTPIUser currentUser].emailId,currentPasswordField.textField.text,newPAsswordField.textField.text];
        [Hud show:YES];
        RTPIWebServiceManager *webServiceManager = [[RTPIWebServiceManager alloc]initWithUrl:url andId:0];
        [webServiceManager setWebServiceDelegate:self];
    }
}
-(void)showAlertWithTitle:(NSString *)title andMessage :(NSString *)message{
    [[[UIAlertView alloc]initWithTitle:title message:message delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
}
-(void)rightBarButtonItemPressed{
    RTPIAppDelegate *appDelgate = [UIApplication sharedApplication].delegate;
    [appDelgate didLogout];
}

-(void)didFinishParsingWithResult:(id)result{
    [Hud hide:YES];
    if ([[result objectForKey:@"Status"]boolValue] ) {

        [self showAlertWithTitle:@"Success" andMessage:@"Password changed"];
    }
    else{
        [self showAlertWithTitle:@"Failure" andMessage:@"Your old Password may be incorrect.Please try again."];
    }
   

}
@end
