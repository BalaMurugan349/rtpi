//
//  RTPIQueryViewController.h
//  RTPI
//
//  Created by apple on 12/7/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTPIQueryViewController : UIViewController
{
    IBOutlet UIButton *button;
}
@property (strong, nonatomic) IBOutlet UITextView *queryTxtView;
- (IBAction)postButtonPressed:(id)sender;
- (IBAction)selectedQueryPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *selectQueryButton;


@end
