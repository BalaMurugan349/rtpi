//
//  RTPINewsDetailsViewController.m
//  RTPI
//
//  Created by apple on 11/6/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import "RTPINewsDetailsViewController.h"

@interface RTPINewsDetailsViewController ()
{
}

@end

@implementation RTPINewsDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
//    CGRect frame =  [UIScreen mainScreen].bounds;
//    self.navigationController.view.frame = CGRectMake(frame.origin.x, frame.origin.y+20, frame.size.width, frame.size.height);

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.webView loadHTMLString:self.htmlString baseURL:nil];
    [self.dateLabel setText:self.dateText];
    [self.titleLabel setText:self.titleText];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 13, 20)];
    
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(backbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *buttn = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = buttn;
}
-(void)backbuttonPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)loadWebViewWithHtmalString :(NSString *)string{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
