//
//  RTPIAppDelegate.h
//  RTPI
//
//  Created by apple on 11/5/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTPIPriceDetails.h"
@interface RTPIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,nonatomic)BOOL isEnteredGraph;
-(void)didLogin_priceDetails:(RTPIPriceDetails *)rtpiPriceDetails priceListDetails:(NSArray *)arrayOfObjetcs;
-(void)didLogout;


//LOGIN
//a1@gmail.com
//888
@end
