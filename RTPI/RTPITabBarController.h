//
//  RTPITabBarController.h
//  RTPI
//
//  Created by apple on 11/6/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTPIPriceDetails.h"
@interface RTPITabBarController : UITabBarController
@property(nonatomic,retain)RTPIPriceDetails *rtpiPriceDetails;
@property(nonatomic,retain)NSArray *arrayOfObjects;
- (instancetype)initWithArray:(NSArray *)array;
@end
