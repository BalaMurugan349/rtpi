//
//  RTPINewsViewController.m
//  RTPI
//
//  Created by apple on 11/6/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import "RTPINewsViewController.h"
#import "RTPINewsDetailsViewController.h"
#import "RTPIWebServiceManager.h"
#import "RTPINews.h"

@interface RTPINewsViewController ()<RTPIWebserviceDelegate>{
    NSMutableArray *newsDetailsArray,*tempNewsArray;
    UIRefreshControl *refreshcontrol;
    MBProgressHUD *hud;
    NSUInteger countDiff;
}

@end

@implementation RTPINewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.newsTableView.separatorColor = [UIColor blackColor];
        [self customiseNavigationBar];
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self fetchNewsFromFromServer_position:0 count:100];

}
#pragma mark - Customise Navigation Bar
-(void)customiseNavigationBar{
   
    self.title = @"Breaking News";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
     hud = [[MBProgressHUD alloc]init];
    newsDetailsArray = [[NSMutableArray alloc]init];
    tempNewsArray = [[NSMutableArray alloc]init];
    [self addRefreshControl];
    [self.navigationController.view addSubview:hud];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:18.0f]};
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.title = @"Breaking News";
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navgtnBackGround.png"] forBarMetrics:UIBarMetricsDefault];
}

-(void)addRefreshControl{
    refreshcontrol = [[UIRefreshControl alloc]init];
    [_newsTableView addSubview:refreshcontrol];
    [refreshcontrol addTarget:self action:@selector(refreshInvoked) forControlEvents:UIControlEventValueChanged];
}

-(void)refreshInvoked{
    [refreshcontrol endRefreshing];
     [self fetchNewsFromFromServer_position:0 count:100];
}
#pragma mark - Fetch News From Server
//http://bc.savola.com/webservice/ListnewsNew.aspx?Position=%d&Count=%d
-(void)fetchNewsFromFromServer_position:(int)position count:(int)count
{
    [hud show:YES];
    //sicsglobal.co.in/solution/rtpi/ListnewsNew.aspx?Position=0&Count=2
    NSString *url    = [NSString stringWithFormat:@"ListnewsNew.aspx?Position=%d&Count=%d",position,count];
    NSLog(@"Url %@",url);
    
    RTPIWebServiceManager *webServiceManager = [[RTPIWebServiceManager alloc]initWithUrl:url andId:0];
    [webServiceManager setWebServiceDelegate:self];
}

#pragma mark - TableViewDelegate And DataSources

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return newsDetailsArray.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *breakingNewsCell;
    static NSString *cellIdentifier = @"PriceDetailCell";
    breakingNewsCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    breakingNewsCell.backgroundColor = [UIColor whiteColor];
    if (!breakingNewsCell)
    {
        breakingNewsCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        
    }
    breakingNewsCell.backgroundView.backgroundColor = [UIColor clearColor];
    [breakingNewsCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self loadDataForCell:breakingNewsCell atIndexPath:indexPath];
    breakingNewsCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [breakingNewsCell setBackgroundColor:[UIColor clearColor]];
    return breakingNewsCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RTPINewsDetailsViewController *newsDetailsViewcontroller = [[RTPINewsDetailsViewController alloc]initWithNibName:@"RTPINewsDetailsViewController" bundle:Nil];
    
    RTPINews *news            = [newsDetailsArray objectAtIndex:indexPath.row];
    newsDetailsViewcontroller.htmlString = news.newsDescription;
    newsDetailsViewcontroller.titleText  = news.newsTitle;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"M/d/yyyy hh:mm:ss a"];
    NSString *dateString=news.newsDate;
    //        [formatter dateFromString:dateString];
    NSDate *dt=[formatter dateFromString:dateString];
    formatter.dateFormat=@"M/d/yyyy hh:mm a";
    NSString *date = [formatter stringFromDate:dt];
    newsDetailsViewcontroller.dateText   = date;
    [self.navigationController pushViewController:newsDetailsViewcontroller animated:YES];
}

#pragma mark - WebServiceDelegate

-(void)didFinishParsingWithResult:(id)result{
    [tempNewsArray removeAllObjects];
    
    for (NSDictionary *dictionary in result) {
       
        RTPINews *newsDetails = [[RTPINews alloc]initWithNewsDictionary:dictionary];
        [tempNewsArray addObject:newsDetails];
         [newsDetailsArray removeAllObjects];
       [newsDetailsArray addObjectsFromArray:[tempNewsArray copy]];
    }
    

    [hud hide:YES];
    
    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"TotalCount"]) {
        NSUInteger defaultValue = [[NSUserDefaults standardUserDefaults]integerForKey:@"TotalCount"];
        if ((defaultValue < newsDetailsArray.count)) {
             countDiff = newsDetailsArray.count - defaultValue;
        }else{
            countDiff=0;
        }
    }else{
        [[NSUserDefaults standardUserDefaults]setInteger:newsDetailsArray.count forKey:@"TotalCount"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    [self.newsTableView reloadData];
}

-(void)loadCellWithDataAtIndexPAth :(int)indxPath{
    
}

#pragma mark - Load Data For Cell
-(void)loadDataForCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    
    /*****************************Set properties of title label******************************/
   // cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:17.0f];
   
    /**************************Set properties of description label**************************/
  //  cell.detailTextLabel.textColor = [UIColor blackColor];
    cell.detailTextLabel.font      = [UIFont systemFontOfSize:12.0f];
    /*********************Set properties of TableView accessory button**********************/
  //  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.accessoryView = [self makeDetailDisclosureButton];
    
    
    RTPINews *news            = [newsDetailsArray objectAtIndex:indexPath.row];
    cell.textLabel.text       = news.newsTitle;
   
    if (indexPath.row<countDiff) {
            cell.textLabel.textColor = [UIColor blueColor];
            cell.detailTextLabel.textColor = [UIColor blueColor];

        }else{
            [[NSUserDefaults standardUserDefaults]setInteger:newsDetailsArray.count forKey:@"TotalCount"];
            [[NSUserDefaults standardUserDefaults]synchronize];

            cell.textLabel.textColor = [UIColor blackColor];
            cell.detailTextLabel.textColor = [UIColor blackColor];
        }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"M/d/yyyy hh:mm:ss a"];
    NSString *dateString=news.newsDate;
    NSDate *dt=[formatter dateFromString:dateString];
    formatter.dateFormat=@"M/d/yyyy hh:mm a";
    NSString *date = [formatter stringFromDate:dt];
    cell.detailTextLabel.text = date;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIButton *) makeDetailDisclosureButton
{
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundColor:[UIColor redColor]];
    
    [button addTarget: self
               action: @selector(accessoryButtonTapped:withEvent:)
     forControlEvents: UIControlEventTouchUpInside];
    
    return (button);
}

- (void) accessoryButtonTapped: (UIControl *) button withEvent: (UIEvent *) event
{
    NSIndexPath * indexPath = [self.newsTableView indexPathForRowAtPoint: [[[event touchesForView: button] anyObject] locationInView: self.newsTableView]];
    if ( indexPath == nil )
        return;
    
    [self.newsTableView.delegate tableView: self.newsTableView accessoryButtonTappedForRowWithIndexPath: indexPath];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if(self.newsTableView.contentOffset.y >= (self.newsTableView.contentSize.height - self.newsTableView.frame.size.height))
    {
       // [self fetchNewsFromFromServer_position:newsDetailsArray.count count:newsDetailsArray.count];
    }
}

@end
