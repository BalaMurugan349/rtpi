//
//  CustomPageView.h
//  RTPI
//
//  Created by apple on 12/18/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomPageViewDelegate <NSObject>
@optional
-(void)didSelectButtonAtIndex :(int)index OfButtonArray:(NSArray *)buttons;
-(void)customViewDidLoadWithButtons:(NSArray *)buttons;

@end

@interface CustomPageView : UIView
-(id)initWithCustomPageViewWithFrame:(CGRect)frame andNumberOfButtons:(CGFloat)number;
-(void)addButtonsToContainerView:(CGFloat)numberOfbuttons;
@property (nonatomic,strong) id<CustomPageViewDelegate> customPageViewDelegate;
@property (nonatomic,strong) NSArray *buttonsArray;
@end
