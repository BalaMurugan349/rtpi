

//
//  main.m
//  RTPI
//
//  Created by apple on 11/5/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RTPIAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RTPIAppDelegate class]));
    }
}
