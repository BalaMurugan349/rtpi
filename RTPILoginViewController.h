//
//  RTPILoginViewController.h
//  RTPI
//
//  Created by apple on 11/5/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTPIPriceDetails.h"
@interface RTPILoginViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *loginTableView;
NSString* sasi(int tr);
@property(nonatomic,retain)RTPIPriceDetails *rtpiPriceDetails;
@property(nonatomic,retain)NSString *deviceToken;
@property(nonatomic,retain)NSArray *arrayOfObjects;
@end
