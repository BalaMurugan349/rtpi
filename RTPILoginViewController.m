//
//  RTPILoginViewController.m
//  RTPI
//
//  Created by apple on 11/5/13.
//  Copyright (c) 2013 Abhishek. All rights reserved.
//

#import "RTPILoginViewController.h"
#import "TextFieldCell.h"
#import "ButtonCell.h"
#import "RTPIAppDelegate.h"
#import "RTPIWebServiceManager.h"
#import "RTPIUser.h"
//#import <Crashlytics/Crashlytics.h>

@interface RTPILoginViewController ()<UITableViewDataSource,UITableViewDelegate,RTPIWebserviceDelegate>
{
    TextFieldCell *userNameField, *passwordField;
    ButtonCell *loginbutton;
    NSArray    *rowsArray;
    MBProgressHUD *Hud;

}

@end

@implementation RTPILoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        [self configureCells];
        [self setUpTableContents];
        [self modifyTableView];
        
    }
    return self;
}

#pragma mark -ConfigureCells

-(void)configureCells
{
    [self configureTextFields];
    [self configureButtons];
    
}
-(void)configureTextFields{
    userNameField = [[TextFieldCell alloc]initWithPlaceholder:@"Email"];
    passwordField = [[TextFieldCell alloc]initWithPlaceholder:@"Password"];
   // userNameField.textField.text = @"a1@gmail.com";
   // passwordField.textField.text = @"888";
    userNameField.backgroundColor = [UIColor clearColor];
    userNameField.textField.backgroundColor = [UIColor whiteColor] ;
    [userNameField.textField setTintColor:[UIColor blackColor]];
    userNameField.textField.keyboardType = UIKeyboardTypeEmailAddress;
     passwordField.backgroundColor = [UIColor clearColor];
      passwordField.textField.backgroundColor = [UIColor whiteColor] ;
    [userNameField.textField.layer setBorderWidth:1];
    [userNameField.textField.layer setCornerRadius:5];
    [passwordField.textField.layer setBorderWidth:1];
    [passwordField.textField.layer setCornerRadius:5];
    [passwordField.textField setTintColor:[UIColor blackColor]];
}
-(void)configureButtons
{
    loginbutton = [[ButtonCell alloc]initWithButtonTitle:@"Log In"];
    [loginbutton.button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  loginbutton.backgroundColor = [UIColor clearColor];
    loginbutton.button1.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    loginbutton.button1.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"logInBtnBackGround.png"]];
    [loginbutton.button1.layer setCornerRadius:5];
    [loginbutton.button1 addTarget:self action:@selector(loginButtonPressed) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - SetUpTablecontents

-(void)setUpTableContents{
    
    rowsArray = @[@[userNameField,passwordField],@[loginbutton]];
    [self.loginTableView reloadData];
}
#pragma mark - ModifyTable
-(void)modifyTableView
{
    [self.loginTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}
#pragma mark - TableViewDataSource and Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return rowsArray.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   return [rowsArray[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //return [[rowsArray ]objectAtIndex:indexPath.row];
    return [rowsArray[indexPath.section]objectAtIndex:indexPath.row];
}
#pragma mark -LoginButtonPressed

-(void)loginButtonPressed{
    if ([self validateLogin]) {
//        if ([self validateEmail:userNameField.textField.text])
//        {
            NSString *url = [NSString stringWithFormat:@"Login.aspx?email=%@&password=%@&devicetoken=%@",userNameField.textField.text,passwordField.textField.text,[[NSUserDefaults standardUserDefaults ]objectForKey:@"deviceToken"]];
            [Hud show:YES];
            RTPIWebServiceManager *webServiceManager = [[RTPIWebServiceManager alloc]initWithUrl:url andId:0];
            [webServiceManager setWebServiceDelegate:self];
  //      }
//        else{
//            [self showAlertWithMessage:@"Please enter a valid email id."];
//        }
        
    }
}
#pragma mark -ValidateLogin

/*****************Check if all fields are filled*************/

-(BOOL)validateLogin{
    
    if (userNameField.textField.text.length > 0 && passwordField.textField.text.length > 0) {
        return YES;
    }
    else
        [self showAlertWithMessage:@"Please fill all fields."];
    return NO;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    Hud = [[MBProgressHUD alloc]init];
    [self.navigationController.view addSubview:Hud];
    [self.navigationController setNavigationBarHidden:YES];
}
NSString* sasi(int tr){
    return @"grrr";
}

#pragma mark - Validate Email
- (BOOL)validateEmail:(NSString *)emailStr {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
    
}
#pragma mark - ShowAlert
-(void)showAlertWithMessage:(NSString*)message{
    [[[UIAlertView alloc]initWithTitle:@"Error" message:message delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    
}
#pragma mark - RTPI WebServiceDelegate
-(void)didFinishParsingWithResult:(id)result{
    [Hud hide:YES];
    if ([[result valueForKey:@"Result"] isKindOfClass:[NSNull class]]) {
        [self showAlertWithMessage:@"Username and password do not match.Please try again."];

    }else{
    if ([[result objectForKey:@"Result"] isEqualToString:@"Success"]) {
        [[NSUserDefaults standardUserDefaults]setValue:result[@"Username"] forKey:@"userName"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [[NSUserDefaults standardUserDefaults]setValue:result[@"email"] forKey:@"emailId"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [[NSUserDefaults standardUserDefaults]setValue:result[@"Id"] forKey:@"userId"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [RTPIUser currentUser].userID = [result objectForKey:@"Id"];
        [RTPIUser currentUser].emailId = [result objectForKey:@"email"];
        [RTPIUser currentUser].password = passwordField.textField.text;
        [RTPIUser currentUser].userName=[result objectForKey:@"Username"];
        [RTPIUser currentUser].emailId=[result objectForKey:@"email"];
        RTPIAppDelegate *delegate = [UIApplication sharedApplication].delegate;
        [delegate didLogin_priceDetails:_rtpiPriceDetails priceListDetails:_arrayOfObjects];
    }
    else if ([[result objectForKey:@"Result"] isEqualToString:@"failed"])
    {
        [self showAlertWithMessage:@"Username and password do not match.Please try again."];
    }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    float height;
    height = 0;
    if (section == 0) {
        height = 135;
    }
    return height;
}


@end
